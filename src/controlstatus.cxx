/**
 * @file controlstatus.cxx
 * @author Ken Teh
 * @date 20231018
 */
#include <sys/socket.h>
#include <poll.h>
#include <csignal>
#include <cerrno>
#include <nlohmann/json.hpp>
#include "controlstatus.h"
#include "fmacp.h"

void from_json(const nlohmann::json &j, CScommand &c)
{
    if (j.find("cmd") == j.end()) {
        c = CScommand();
        return;
    }
    c.cmd = j.at("cmd").get<std::string>();
    if (j.find("params") == j.end()) {
        c.params = "";
        return;
    }
    c.params = j["params"].is_string() ? j["params"].get<std::string>()
                : j["params"].dump();
}

ControlStatus::ControlStatus(const char *nm_, const mqtt::ConnectData &mqttdata_,
        device_map_type &devices_) :
    mqtt::async_client(mqttdata_.brokerURI, nm_),
    ktool::kthread<ControlStatus>(nm_), nm(nm_), mqttdata(mqttdata_),
    devices(devices_), rsk(-1), wsk(-1), logr(spdlog::get(LOGRNAME))
{
    if (!logr) throw std::runtime_error(
            fmt::format("{}/{}: cannot find logger \"{}\"",
                __FILE__, __LINE__, LOGRNAME));

    /* The socketpair is a trick to make the controlstatus thread more
     * responsive to external input. Stopping the thread or losing 
     * connection to the mqtt broker will close the write-end of the
     * socketpair.  By polling on the read-end, the thread will be able to
     * detect a change in state quickly.
     */
    int sa[2];
    if (socketpair(PF_LOCAL, SOCK_STREAM, 0, sa) == -1)
        throw std::runtime_error(
                fmt::format("{}/{}: create socketpair failed <{}>",
                    __FILE__, __LINE__, errno));
    rsk = sa[0];
    wsk = sa[1];

    using namespace std::placeholders;
    auto fconnected = std::bind(&ControlStatus::on_connected, this, _1);
    set_connected_handler(fconnected);
    auto fconnection_lost = std::bind(&ControlStatus::on_connection_lost,
                                this, _1);
    set_connection_lost_handler(fconnection_lost);
    auto fmessage = std::bind(&ControlStatus::on_message, this, _1);
    set_message_callback(fmessage);
}


void ControlStatus::on_connected(const std::string &s)
{
    logr->info("{} connected to mqtt broker at {}, s=\"{}\"",
            nm, mqttdata.brokerURI, s);
}

void ControlStatus::on_connection_lost(const std::string &s)
{
    logr->warn("{} lost connection to broker, s=\"{}\"", nm, s);
    if (wsk != -1) { close(wsk); wsk = -1; }
}

void ControlStatus::on_message(mqtt::const_message_ptr m)
{
    using json = nlohmann::json;

    std::string payload = m->get_payload_str();
    logr->debug("{} message arrived \"{}\"", nm, payload);
    json j = json::parse(payload);
    CScommand c = j.get<CScommand>();
    logr->info("{} c.cmd={}, c.params={}", nm, c.cmd, c.params);
    if (c.cmd == "shutdown") {
        for (auto &d : devices) {
            switch (d.second.index()) {
                case 0:
                    std::get<0>(d.second)->shutdown();
                    break;
                case 1:
                    std::get<1>(d.second)->shutdown();
            }
        }
    }
}

void ControlStatus::start()
{
    using namespace std::chrono_literals;

    if (is_connected()) return;
    auto connect_opts = mqtt::connect_options_builder()
        .clean_session()
        .connect_timeout(BROKER_CONNECT_TIMEOUT)
        .finalize();

    if (mqttdata.secure) {
        if (mqttdata.cafile.empty() or mqttdata.username.empty() 
                or mqttdata.userpasswd.empty())
            throw std::runtime_error(fmt::format("{}/{}: missing cafile "
                        "or username oruserpasswd for mqtts connection."
                        __FILE__, __LINE__));
        auto ssl_opts = mqtt::ssl_options_builder()
            .trust_store(mqttdata.cafile)
            .enable_server_cert_auth(true)
            .verify(true)
            .finalize();
        connect_opts.set_ssl(ssl_opts);
        connect_opts.set_user_name(mqttdata.username);
        connect_opts.set_password(mqttdata.userpasswd);
    }

    if (!connect(connect_opts)->wait_for(BROKER_CONNECT_TIMEOUT))
        throw std::runtime_error(fmt::format(
                    "{}/{}: connect to \"{}\" timed out",
                    __FILE__, __LINE__ , mqttdata.brokerURI));

    kthread_type::start();
}

void ControlStatus::stopjoin()
{
    /* Close the socketpair write end. If threadfn is polling on the read
     * end, this will make poll() return immediately.
     */
    if (wsk != -1) { close(wsk); wsk = -1; }
    kthread_type::stopjoin();
    if (is_connected()) {
        try { disconnect(); }
        catch (std::exception&) {}
    }
}

void ControlStatus::threadfn()
{
    using namespace std::chrono_literals;
    sigset_t mask;

    logr->info("{} thread started", nm);
    sigfillset(&mask);
    pthread_sigmask(SIG_BLOCK, &mask, nullptr);

    if (!subscribe(CONTROLSTATUS_TOPIC, 1)
            ->wait_for(BROKER_CONNECT_TIMEOUT)) {
        logr->error("{} subscribe {} failed",
                nm, CONTROLSTATUS_TOPIC);
        kthread_type::exited = true;
        return;
    }
    logr->info("{} subscribed to {} at {}", nm, CONTROLSTATUS_TOPIC,
            mqttdata.brokerURI);

    bool again = true;
    pollfd pfd[] = {{ rsk, POLLIN, 0 }};
    int poll_timeout = 30000;       // 30s
    while (again) {
        /* The controlstatus thread polls on the read-end of the
         * socketpair. The timeout is set large since the poll will return
         * immediately when the state of the socketpair changes. This way,
         * this while loop does not spin but still retains immediate
         * response to external input.
         */
        int npolled;

        if (stop_requested) {
            logr->info("{} stop requested", nm);
            break;
        }
        
        npolled = poll(pfd, 1, poll_timeout);
        if (npolled == 0) continue;
        if (npolled == -1) logr->error("{} poll error <{}>", nm, errno);
        again = false;
    }

    unsubscribe(CONTROLSTATUS_TOPIC);
    if (wsk != -1) { close(wsk); wsk = -1; }
    if (rsk != -1) { close(rsk); rsk = -1; }
    kthread_type::exited = true;
    logr->info("{} thread exited", nm);
}
