/**
 * @author Ken Teh
 * @date 20220211
 */
#include "serial.h"
#include <fmt/format.h>
#include <fcntl.h>
#include <cerrno>
#include <unistd.h>
#include <chrono>

void Serial::open_device(const char *devnm_, speed_t baud, tcflag_t nbits,
        tcflag_t cstopb, tcflag_t parenb, tcflag_t parodd)
{
    int flags = O_RDWR | O_NOCTTY;
    if ((fd = ::open(devnm_, flags)) == -1) 
        throw std::runtime_error(fmt::format("{}: open('{}') error <{}>",
                    __func__, devnm_, errno));

    if (tcgetattr(fd, &ta0) == -1) {
        close_device();
        throw std::runtime_error(fmt::format("{}: tcgetattr '{}' <{}>",
                    __func__, devnm_, errno));
    }

    termios ta = ta0;
    cfmakeraw(&ta);
    ta.c_iflag = 0;
    ta.c_oflag = 0;
    ta.c_lflag &= ~ICANON;
    ta.c_cflag = nbits|cstopb|parenb|parodd|CREAD|CLOCAL;
    ta.c_cc[VMIN] = 1;
    ta.c_cc[VTIME] = 0;
    if (cfsetispeed(&ta, baud) == -1 or cfsetospeed(&ta, baud) == -1) {
        close_device();
        throw std::runtime_error(fmt::format("{}: set baud '{}' <{}>",
                    __func__, devnm_, errno));
    }
    if (tcsetattr(fd, TCSANOW, &ta) == -1) {
        close_device();
        throw std::runtime_error(fmt::format("{}: tcsetattr '{}' <{}>",
                    __func__, devnm_, errno));
    }
    tcflush(fd, TCIOFLUSH);
    
    tset = true;
    devnm = devnm_;
}

void Serial::close_device()
{
    if (fd >= 0) {
        if (tset) tcsetattr(fd, TCSANOW, &ta0);
        ::close(fd);
        fd = -1;
    }
}

Serial::wret_type Serial::write(const void *buf, size_t sz, int msecs)
{
    if (fd < 0)
        return wret_type(0, std::error_condition(EBADF,
                    std::system_category()));

    std::error_condition ecnd;
    std::unique_lock<Serial> k(*this, std::defer_lock);
    bool locked;

    locked = msecs < 0 ? k.lock(), true :
                k.try_lock_for(std::chrono::milliseconds(msecs));
    if (!locked)
        return wret_type(0, std::error_condition(ETIMEDOUT,
                    std::system_category()));


    const char *b = static_cast<const char*>(buf);
    size_t nleft=sz;
    while (nleft > 0) {
        ssize_t nwr;
        if ((nwr = ::write(fd, b, nleft)) == -1) {
            ecnd.assign(errno, std::system_category());
            break;
        }
        b += nwr;
        nleft -= nwr;
    }

    return wret_type(sz-nleft, ecnd);
}

Serial::rret_type Serial::readall(int msecs)
{
    if (fd < 0)
        return rret_type(std::string(), std::error_condition(EBADF,
                    std::system_category()));

    std::unique_lock<Serial> k(*this, std::defer_lock);
    bool locked;

    locked = msecs < 0 ? k.lock(), true :
                k.try_lock_for(std::chrono::milliseconds(msecs));
    if (!locked)
        return rret_type(std::string(), std::error_condition(ETIMEDOUT,
                    std::system_category()));

    /* Read one char at a time. Quit if there is nothing to read after poll
     * times out. Wait time needs to be tuned to the device.  In theory,
     * the wait time should be approximately equal to the terminal baud
     * rate. At 9600baud this should be about 1ms.  But, it really depends
     * on when the actual serial device begins its response.
     */
    int pmsecs = poll_msecs;
    pfd.fd = fd;
    pfd.events = POLLIN;
    rret_type ret;
    while (true) {
        int npolled;
        ssize_t nrd;
        char c;

        npolled = poll(&pfd, 1, pmsecs);
        if (npolled == 0) break;
        if ((nrd = ::read(fd, &c, 1)) == -1) {
            ret.second.assign(errno, std::system_category());
            return ret;
        }
        if (nrd == 1) {
            ret.first.append(1, c);
        }
    }

    return ret;
}
