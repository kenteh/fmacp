#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/spdlog.h>
#include <thread>
#include <chrono>
#include "camac.h"

auto logr = spdlog::stderr_logger_mt("logr");

/* Check opening the camac controller.
 */
int func0()
{
    try {
        /* ANLPHY KS3989's are strapped at 9600 baud, 7 bits, with 1 stop
         * bits, parity disabled.
         */
        auto ser = std::make_shared<Serial>("/dev/ttyUSB0", B9600,
                    CS7, CSTOPB);
        logr->info("/dev/ttyUSB0 opened? {}", bool(*ser));

        auto cc = std::make_shared<KS3989>(ser);
        auto rc = cc->get_csr();
        if (std::holds_alternative<std::error_condition>(rc)) {
            auto ec = std::get<std::error_condition>(rc);
            logr->error("error={}, \"{}\"", ec.value(), ec.message());
        }
        else {
            logr->info("csr=0x{:08x}, should be 0x408", std::get<int>(rc));
        }
    }
    catch (std::exception &e) {
        logr->error("exception - {}", e.what());
    }

    return 0;
}


/* Talking with a PS7164, pt.1  This one just nafs it so we get see the N
 * light on the module.
 */
int func1()
{
    using namespace std::chrono_literals;
    serial_ptr ser;
    ks3989_ptr cc;

    try {
        ser = std::make_shared<Serial>("/dev/ttyUSB0", B9600,
                    CS7, CSTOPB);
        cc = std::make_shared<KS3989>(ser);
    }
    catch (std::exception &e) {
        logr->error("exception - {}", e.what());
        return 1;
    }

    /* The phillips 7164 is in slot 5.  Read its control register.
     */
    KS3989::ioget_type rc;
    int *vint;
    std::error_condition ec;
    for (int i = 0; i < 10; ++i) {
        rc = cc->get(5, 0, 6);
        vint = std::get_if<int>(&rc);
        if (vint == nullptr) {
            // null means it's not an int, ie, it's an error_condition.
            ec = std::get<std::error_condition>(rc);
            logr->error("get(5,0,6) returns error={}, \"{}\"",
                    ec.value(), ec.message());
        }
        else {
            logr->info("get(5,0,6) returns 0x{:08x}", *vint);
        }
        std::this_thread::sleep_for(2s);
    }

    return 0;
}

/* Talking with a PS7164, pt.2
 */
int func2()
{
    using namespace std::chrono_literals;
    serial_ptr ser;
    ks3989_ptr cc;

    try {
        ser = std::make_shared<Serial>("/dev/ttyUSB0", B9600,
                    CS7, CSTOPB);
        cc = std::make_shared<KS3989>(ser);
    }
    catch (std::exception &e) {
        logr->error("exception - {}", e.what());
        return 1;
    }

    /* The phillips 7164 is in slot 5.  Read its control register.
     */
    KS3989::ioget_type rc;
    std::error_condition ec;
    rc = cc->get(5, 0, 6);
    if (std::holds_alternative<std::error_condition>(rc)) {
        ec = std::get<std::error_condition>(rc);
        logr->error("get(5,0,6) returns error={}, \"{}\"",
                ec.value(), ec.message());
        return 1;
    }
    logr->info("get(5,0,6) returns 0x{:08x}", std::get<int>(rc));

    /* Reset it and read the control register again.
     */
    ec = cc->set(5, 0, 11, 0);
    if (bool(ec)) {
        logr->error("set(5,0,11) returns error={}, \"{}\"",
                ec.value(), ec.message());
        return 1;
    }
    rc = cc->get(5, 0, 6);
    if (std::holds_alternative<std::error_condition>(rc)) {
        ec = std::get<std::error_condition>(rc);
        logr->error("get(5,0,6) returns error={}, \"{}\"",
                ec.value(), ec.message());
        return 1;
    }
    logr->info("get(5,0,6) should return 0, -> 0x{:08x}",
            std::get<int>(rc));

    /* Set all bits in the control register.
     */
    ec = cc->set(5, 0, 19, 7);
    if (bool(ec)) {
        logr->error("set(5,0,19,7) returns error={}, \"{}\"",
                ec.value(), ec.message());
        return 1;
    }

    rc = cc->get(5, 0, 6);
    if (std::holds_alternative<std::error_condition>(rc)) {
        ec = std::get<std::error_condition>(rc);
        logr->error("get(5,0,6) returns error={}, \"{}\"",
                ec.value(), ec.message());
        return 1;
    }

    logr->info("get(5,0,6) should return 7, x=0x{:08x}",
            std::get<int>(rc));

    return 0;
}


/* Run a digital test cycle on the 7164.
 */
int func3()
{
    using namespace std::chrono_literals;
    serial_ptr ser;
    ks3989_ptr cc;

    try {
        ser = std::make_shared<Serial>("/dev/ttyUSB0", B9600,
                    CS7, CSTOPB);
        cc = std::make_shared<KS3989>(ser);
    }
    catch (std::exception &e) {
        logr->error("exception - {}", e.what());
        return 1;
    }

    std::error_condition ec;
    KS3989::ioget_type rc;
    ec = cc->set(5, 0, 9, 0);
    if (bool(ec)) {
        logr->error("set(5,0,9) returns error={}, \"{}\"",
                ec.value(), ec.message());
        return 1;
    }
    
    ec = cc->set(5, 4, 17, 0);
    if (bool(ec)) {
        logr->error("set(5,4,17) returns error={}, \"{}\"",
                ec.value(), ec.message());
        return 1;
    }
    ec = cc->set(5, 0, 20, 0);
    if (bool(ec)) {
        logr->error("set(5,0,20) returns error={}, \"{}\"",
                ec.value(), ec.message());
        return 1;
    }
    ec = cc->set(5, 0, 25, 0);
    if (bool(ec)) {
        logr->error("set(5,0,25) returns error={}, \"{}\"",
                ec.value(), ec.message());
        return 1;
    }
    std::this_thread::sleep_for(1s);
    for (int i = 0; i < 16; ++i) {
        int j;
        rc = cc->get(5, i, 0);
        if (std::holds_alternative<std::error_condition>(rc)) {
            ec = std::get<std::error_condition>(rc);
            logr->error("get(5, {}, 0) returns error {} \"{}\"",
                    i, ec.value(), ec.message());
            return 1;
        }
        j = (i << 12) | 0x249;
        logr->info("get(5,{},0) expect 0x{:04x} -> 0x{:04x}",
                i, j, std::get<int>(rc));
    }

    return 0;
}


int main(int argc, char *argv[])
{
    unsigned long x = argc > 1 ? std::stoul(argv[1]) : 0ul;
    logr->set_pattern("%v");
    logr->set_level(spdlog::level::info);

    std::vector<std::function<int()>> funcs { func0, func1, func2,
        func3 };
    return x < funcs.size() ? funcs[x]() : 0;
}
