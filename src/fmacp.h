/**
 * @file fmacp.h
 * @author Ken Teh
 * @date 20230105
 */
#ifndef _fmacp_h
#define _fmacp_h

#define LOGRNAME                    "logr"
#define BROKER_CONNECT_TIMEOUT      3s
#define CAMAC_LOCK_TIMEOUT          200ms
#define CAMAC_STOP_TIMEOUT          3s
#define PUBLISH_TIMEOUT             200ms

#define CONTROLSTATUS_TOPIC         "fma/controlstatus"

/* Device status latency (min,max) in milliseconds in mock mode.
 * In true mode, the device status is read every loop iteration. Any
 * latency is hardware I/O latency. In mock mode, this latency is mocked
 * with a uniform random distribution between the following min and max.
 */
#define MOCK_STATUS_LATENCY_MIN     1000
#define MOCK_STATUS_LATENCY_MAX     2000
#endif
