/**
 * @author Ken Teh
 * @date 20220218
 */
#pragma once
#include "serial.h"


class Blackbox : private virtual Serial {
    public:
        Blackbox(unsigned int numports_, const char *devnm,
                speed_t baud=B9600, tcflag_t nbits=CS8,
                tcflag_t cstopb=0, tcflag_t parenb=0, tcflag_t parodd=0,
                int poll_msecs=40) :
            Serial(devnm, baud, nbits, cstopb, parenb, parodd, poll_msecs)
            {}
        Blackbox() : Serial() {}
        ~Blackbox() {}

        /**
         * Get/set the I/O port.
         */
        unsigned int report(unsigned int port_) override;
        unsigned int report() const override { return port; }

    private:
        Blackbox(const Blackbox&) = delete;
        Blackbox &operator=(const Blackbox&) = delete;
};
