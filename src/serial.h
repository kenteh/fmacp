/**
 * @author Ken Teh
 * @date 20220211
 */
#pragma once
#include <mutex>
#include <termios.h>
#include <poll.h>
#include <utility>
#include <system_error>
#include <string>
#include <atomic>
#include <memory>

class Serial : public std::recursive_timed_mutex {
    public:
        typedef std::pair<size_t, std::error_condition> wret_type;
        typedef std::pair<std::string, std::error_condition> rret_type;


        /**
         * Constructor
         * @param devnm_ Device filename
         * @param baud Baud rate
         * @param nbits Number of bits per char.
         * @param cstopb Number of stop bits.
         * @param parenb Enable parity
         * @param parodd Odd parity
         * @param poll_msecs_ Pool timeout in milliseconds.
         *
         * See termios.h for arguments related to a serial device.
         */
        Serial(const char *devnm_, speed_t baud=B9600, tcflag_t nbits=CS8,
                tcflag_t cstopb=0, tcflag_t parenb=0, tcflag_t parodd=0,
                int poll_msecs_=40) : 
            devnm(), fd(-1), ta0(), tset(false), pfd(),
            poll_msecs(poll_msecs_), numports(1), port(0)
        { open_device(devnm_, baud, nbits, cstopb, parenb, parodd); }

        Serial() : 
            devnm(), fd(-1), ta0(), tset(false), pfd(), poll_msecs(-1),
            numports(1), port(0) {}
        virtual ~Serial() { close_device(); }

        std::string name() const { return devnm; }

        /**
         * Change poll timeout
         * @param msecs Poll timeout in milliseconds, -1 to block.
         */
        virtual void set_poll_timeout(int msecs) { poll_msecs = msecs; }

        /**
         * True if serial device is opened.
         */
        virtual operator bool() const { return fd >= 0; }

        /**
         * Write to serial device.
         * @param wbuf Write buffer
         * @param wsz Number of bytes to write.
         * @param msecs Mutex lock timeout in milliseconds, -1 to block.
         * @returns the number of bytes written or an error_condition.
         */
        virtual wret_type write(const void *wbuf, size_t wsz, int
                msecs=-1);

        /**
         * Read from a serial device.
         * @param msecs  Mutex lock timeout in milliseconds, -1 to block
         * @returns the bytes read or an error_condition.
         *
         * Method polls device for input and reads one character at a
         * time. It returns when a poll times out.
         */
        virtual rret_type readall(int msecs=-1);

        /**
         * Set port
         * @param port New port
         * @returns the previous port
         *
         * Adds support for the blackbox device or any multiport serial
         * device.
         */
        virtual unsigned int report(unsigned int port_) { return port; }

        /**
         * Get current port
         * @param port New port
         * @returns the previous port
         */
        virtual unsigned int report() const { return port; }


    protected:
        std::string devnm;
        int fd;
        termios ta0;
        bool tset;
        pollfd pfd;
        std::atomic_int poll_msecs;
        unsigned int numports;
        unsigned int port;

        Serial(const Serial&) = delete;
        Serial &operator=(const Serial&) = delete;
        virtual void open_device(const char*, speed_t, tcflag_t, tcflag_t,
                tcflag_t, tcflag_t);
        virtual void close_device();
}; 
using serial_ptr = std::shared_ptr<Serial>;
