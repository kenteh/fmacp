/**
 * @file dtypes.h
 * @author Ken Teh
 * @date 20231001
 */
#ifndef _dtypes_h
#define _dtypes_h
#include "edipole.h"
#include "magnet.h"

#ifdef MOCKED
/* Device types.
 */
using electric_dipole_type = CamacElectricDipole<MockCamacController>;
using electric_dipole_ptr = std::unique_ptr<electric_dipole_type>;
using camac_magnet_type = CamacMagnet<MockCamacController>;
using camac_magnet_ptr = std::unique_ptr<camac_magnet_type>;

#else
using electric_dipole_type = CamacElectricDipole<KS3989>
using electric_dipole_ptr = std::unique_ptr<electric_dipole_type>;
using camac_magnet_type = CamacMagnet<KS3989>;
using camac_magnet_ptr = std::unique_ptr<camac_magnet_type>;

#endif

/* Organize devices into a container
 */
using device_type = std::variant<electric_dipole_ptr, camac_magnet_ptr>;
using device_map_type = std::unordered_map<std::string, device_type>;



#endif
