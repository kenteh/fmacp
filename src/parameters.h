/**
 * @file parameters.h
 * @author Ken Teh
 * @date 20220520
 */
#ifndef _parameters_h
#define _parameters_h 1
#include <spdlog/spdlog.h>
#include <type_traits>
#include "camac.h"
#include "fmacp.h"
#include <functional>

#if 0
struct MagnetParams {
    inline static const char *labels[] {
        "bgoal", "bmeas", "iprog", "imeas", "vmeas", "cmeas", nullptr };
    enum { bgoal, bmeas, iprog, imeas, vmeas, cmeas, numparams };
};

struct ElectrodeParams {
    inline static const char *labels[] {
        "vgoal", "vprog", "vmeas", "iprog", "imeas", nullptr };
    enum { vgoal, vprog, vmeas, iprog, imeas, numparams };
};
#endif


/* The T here is the camac controller's data_type; either a
 * normal_distribution<>::param_type (for MockCamacController) or an int
 * otherwise.
 */

#if 1
template<typename T,
    typename C = std::conditional_t<std::is_same<T,
                    MockCamacController::data_type>::value,
                    mock_camac_controller_ptr, m3989_ptr>>
#else
template<typename T,
    typename C = std::conditional_t<std::is_same<T,
                    MockCamacController::data_type>::value,
                    mock_camac_controller_ptr, ks3989_ptr>>
#endif
class CamacParam {
public:
    typedef T data_type;
    typedef std::variant<T, std::error_condition> param_type;

    CamacParam()
        : nm(), cc(nullptr), n(-1), a(-1), f(-1), daf(nullptr),
        adf(nullptr), logr() {}
    CamacParam(const char *nm_, C cc_, int n_, int a_, int f_,
            std::function<float(int)> daf_,
            std::function<int(float)> adf_) :
        nm(nm_), cc(cc_), n(n_), a(a_), f(f_), daf(daf_), adf(adf_),
        logr(spdlog::get(LOGRNAME))
    {
        if (!logr)
            throw std::runtime_error(fmt::format(
                        "{}/{}: cannot find logger \"{}\"",
                        __FILE__, __LINE__, LOGRNAME));
        logr->debug("CamacParam<T>::CamacParam(\"{}\",cc@{},{},{},{},"
                "...)@{}", nm, fmt::ptr(cc.get()), n, a, f, fmt::ptr(this));
    }
    ~CamacParam()
    {
        logr->debug("CamacParam<T>::~CamacParam()@{}, nm={}",
                fmt::ptr(this), nm);
    }

    param_type get(int msecs=-1) { return cc->get(n, a, f, msecs); }
    std::error_condition set(const T &t, int msecs=-1) {
        return cc->set(n, a, f, t, msecs);
    }

    const std::string info() {
        return fmt::format("CamacParam(\"{}\", cc@{}, n,a,f=({},{},{})",
            nm, fmt::ptr(cc.get()), n, a, f);
    }

private:
    std::string nm;
    C cc;
    int n;
    int a;
    int f;
    std::function<float(int)> daf;
    std::function<int(float)> adf;
    std::shared_ptr<spdlog::logger> logr;
};


#endif
