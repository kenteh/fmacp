/**
 * @file poletrode.h
 * @author Ken Teh
 * @date 20230106
 */
#pragma once
#include <spdlog/spdlog.h>
#include "parameters.h"
#include "fmacp.h"


/* Like CamacParam<T>, the T is camac controller's data_type, either a
 * normal_distribution<>::param_type or an int.
 *
 * TODO: Replace ks3989_ptr_type with a camac controller base class type?
 */
#if 1
template<typename T,
    typename C = std::conditional_t<std::is_same<T,
                    MockCamacController::data_type>::value,
                    mock_camac_controller_ptr, m3989_ptr>>
#else
template<typename T,
    typename C = std::conditional_t<std::is_same<T,
                    MockCamacController::data_type>::value,
                    mock_camac_controller_ptr, ks3989_ptr>>
#endif
class CamacPoleTrode {
public:
    typedef T value_type;
    typedef std::unordered_map<std::string, T> values_type;
    typedef CamacParam<T> camac_param_type;
    typedef std::pair<std::string, std::error_condition> ioret_type;
    typedef std::unordered_map<std::string, camac_param_type> params_type;

    /**
     * @brief CamacPoleTrode<T> ctor.
     * @param[in] nm            pole or electrode name.
     * @param[in] csparams      map<param_name, camac_param_type>
     * @param[in] cc            ptr to camac controller instance
     *
     * - the camac_param_type defines the NAF parameters that read/write an
     *   electrode parameter, eg, vmeas, iprog, etc.
     * - the camac_controller ptr, cc, is needed only if we want to lock
     *   the controller for exclusive read/write access.
     */
    CamacPoleTrode(const char *nm_, params_type &&csparams_, C cc_=nullptr)
        : nm(nm_), csparams(std::move(csparams_)), cc(cc_),
        logr(spdlog::get(LOGRNAME)) 
    {
        if (!logr)
            throw std::runtime_error(fmt::format(
                        "{}/{}: cannot find logger \"{}\"",
                        __FILE__, __LINE__, LOGRNAME));
        if (logr->level() <= spdlog::level::debug) {
            std::string s;

            s = fmt::format("CamacPoleTrode(nm={}, csparams=[", nm);
            for (auto &kv : csparams)
                s.append(fmt::format("\n {},", kv.second.info()));
            if (cc != nullptr)
                s.append(fmt::format("],\n cc@{})@{}",
                            fmt::ptr(cc.get()), fmt::ptr(this)));
            else
                s.append(fmt::format("])@{}", fmt::ptr(this)));
            logr->debug(s.c_str());
        }
    }
    ~CamacPoleTrode()
    {
        logr->debug("~CamacPoleTrode()@{}, nm={}", fmt::ptr(this), nm);
    }

    std::string name() const { return nm; }

    /**
     * @brief Read pole/electrode parameters.
     * @return parameter values, errors are returned through the call
     *         argument rc.
     *
     * NOTE it only reads parameters suffixed by 'meas'.  The error return
     * typem, ioret_type, is a pair<param_name, error_condition>.
     */
    const values_type& status(ioret_type &rc)
    {
        logr->trace("{} status()", nm);
        rc.first.clear();
        rc.second.clear();
        for (auto &kv : csparams) {
            /* pv is type ioget_type of the CAMAC controller class which is
             * a variant<camac_controller::data_type, error_condition>.
             * See CamacParam<T>::get().  The -1 argument is msecs=-1.
             */
            decltype(kv.second.get(-1)) pv;
            std::string_view suffix;

            suffix = kv.first.c_str() + 1;
            if (suffix != "meas") continue;

            pv = kv.second.get();
            if (std::holds_alternative<std::error_condition>(pv)) {
                rc.first = kv.first;            // param name
                rc.second = std::get<1>(pv);    // get() error_condition
                return values;
            }
            values[kv.first] = std::get<0>(pv); // param = data_type value
        }

        return values;
    }


    /**
     * @brief Write new paramter values to pole/electrode.
     * @param[in] newvalues     new pole/electrode parameter settings.
     * @return pair<param_name, error_condition>
     *
     * The return value is empty if there are no values else it contains
     * parameter and its error trying to write the new setting.
     */
    ioret_type control(const values_type &newvalues)
    {
        ioret_type r = std::make_pair<>(std::string(),
                std::error_condition());

        logr->info("{} control(): set {} new values",
                nm, newvalues.size());
        for (const auto &kv : newvalues) {
            std::string_view suffix;

            suffix = kv.first.c_str() + 1;
            if (suffix == "meas" or
                    (csparams.find(kv.first) == csparams.end()))
                continue;

            /* Set parameter prog/goal to specified value. If set() fails,
             * store the error and param name in r and return.  camac.h
             * defines a fmt::formatter<> struct which lets me print a
             * normal_distribution::param_type.
             */
            logr->info("{} control(): set {}={}",
                    nm, kv.first, kv.second);
            r.second = csparams[kv.first].set(kv.second);
            if (r.second) {
                r.first = kv.first;
                break;
            }
            values.insert_or_assign(kv.first, kv.second);
        }

        return r;
    }

    CamacPoleTrode() = delete;
    CamacPoleTrode(const CamacPoleTrode&) = delete;
    CamacPoleTrode &operator=(const CamacPoleTrode&) = delete;

private:
    std::string nm;
    params_type csparams;
    C cc;
    values_type values;
    std::shared_ptr<spdlog::logger> logr;
};
