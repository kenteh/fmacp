/**
 * @file alistener.cxx
 * @author Ken Teh
 * @date 20230224
 */
#include "alistener.h"
#include "utils.h"


std::string AListener::token_details(const mqtt::token &t)
{
    std::string details;

    if (t.is_complete()) {
        auto rtc = t.get_return_code();
        auto rsc = int(t.get_reason_code());    // 20230927: ReasonCode is
                                                // an enum and fmtlib can
                                                // no longer format enum's.
        details.append(
                fmt::format(", return code=({} \"{}\"), "
                    "reason code=({}, \"{}\")",
                    rtc, mqtt::exception::error_str(rtc),
                    rsc, mqtt::exception::reason_code_str(rsc)));
    }
    else {
        details.append(", token is not complete?");
    }
    return details;
}

void AListener::on_success(const mqtt::token &t)
{
    auto ttype = t.get_type();

    switch (ttype) {
        case mqtt::token::UNSUBSCRIBE:
            {
                auto topics = mqtt::vectorize_string_collection(
                        *(t.get_topics()));
                logr->warn("AListener::on_success(UNSUBSCRIBE): "
                        "nm={}, token: topics=[{}]",
                        nm, fmt::join(topics, ","));
            }
            break;
        case mqtt::token::PUBLISH:
            {
                char *pbuf = static_cast<char*>(t.get_user_context());
                if (pbuf != nullptr) {
                    logr->trace("AListener::on_success(PUBLISH): "
                            "nm={} published \"{}\"", nm, pbuf);
                    delete [] pbuf;
                }
            }
            break;
        case mqtt::token::DISCONNECT:
            logr->warn("AListener::on_success(DISCONNECT): "
                    "nm={}", nm);
            break;
        default:
            logr->warn("AListener::on_success({}): nm={}, "
                    "token ignored", mqtt::tokentypes[ttype], nm);
            break;
    }
}

void AListener::on_failure(const mqtt::token &t)
{
    auto ttype = t.get_type();

    switch (ttype) {
        case mqtt::token::UNSUBSCRIBE:
            {
                auto topics = mqtt::vectorize_string_collection(
                        *(t.get_topics()));
                auto warning = fmt::format("AListener::on_failure"
                        "(UNSUBSCRIBE): nm={}, topics=[{}]",
                        nm, fmt::join(topics, ","));
                warning.append(token_details(t));
                logr->warn("{}", warning);
            }
            break;
        case mqtt::token::PUBLISH:
            {
                char *pbuf = static_cast<char*>(t.get_user_context());
                if (pbuf != nullptr) {
                    auto warning = fmt::format("AListener::on_failure"
                            "(PUBLISH): nm={}, pbuf=\"{}\"", nm, pbuf);
                    warning.append(token_details(t));
                    logr->warn("{}", warning);
                    delete [] pbuf;
                }
            }
            break;
        case mqtt::token::DISCONNECT:
            logr->warn("AListener::on_failure(DISCONNECT): "
                    "nm={}", nm);
            break;
        default:
            logr->warn("AListener::on_failure({}): nm={}, "
                    "token ignored", mqtt::tokentypes[ttype], nm);
            break;
    }
}
