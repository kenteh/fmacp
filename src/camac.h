/**
 * @file camac.h
 * @author Ken Teh
 * @date 20211001
 */
#pragma once
#include <spdlog/spdlog.h>
#include <fmt/format.h>
#include <mutex>
#include <random>
#include <variant>
#include <system_error>
#include <memory>
#include <regex>
#include <nlohmann/json.hpp>
#include "serial.h"
#include "fmacp.h"


class MockCamacController : public std::recursive_timed_mutex {
public:
    typedef typename std::normal_distribution<float>::param_type data_type;
    typedef std::variant<data_type, std::error_condition> ioget_type;
    typedef std::normal_distribution<float> rg_dist_type;

    struct StopParam { int n; int a; int f; data_type d; };

    MockCamacController()
        : std::recursive_timed_mutex(), g(), rg(),
        logr(spdlog::get(LOGRNAME)) 
    {
        if (!logr)
            throw std::runtime_error(fmt::format(
                        "{}/{}: cannot find logger \"{}\"",
                        __FILE__, __LINE__, LOGRNAME));
        logr->debug("MockCamacController()@{}", fmt::ptr(this));
    }
    ~MockCamacController()
    {
        logr->debug("~MockCamacController()@{}", fmt::ptr(this));
    }

    ioget_type get(int n, int a, int f, int msecs=-1);
    std::error_condition set(int n, int a, int f, const data_type &d,
            int msecs=-1);
    bool stopall(const std::vector<StopParam>&);

    constexpr const char *ccname() const { return "MockCamacController"; }

    //MockCamacController() = delete;
    MockCamacController(const MockCamacController&) = delete;
    MockCamacController &operator=(const MockCamacController &) = delete;

private:
    std::minstd_rand g;
    std::unordered_map<int, rg_dist_type> rg;   // normal-dist generators
                                                // indexed by a hash of
                                                // n,a,f.

    std::shared_ptr<spdlog::logger> logr;

    /* Computes a hash of (n,a,f) to be used as an index to rg. Note that
     * f=16..23 is mapped to f=0..7.  This has the effect that naf=(1,0,16)
     * sets the generator parameters for naf=(1,0,0).
     */
    int rgindex(int f, int n, int a) const {
        if (!(((0 <= f && f < 32) && (1 <= n && n < 32))
                    && (0 <= a && a < 16)))
            throw std::invalid_argument(
                    fmt::format("naf=({},{},{}) - invalid argument(s)",
                        n, a, f));

        int f0 = f, idx = 0;
        if (16 <= f && f < 24) f -= 16; // map f(16..23) to f(0..7)
        idx = (f << 9) + ((n-1) << 4) + a;
        logr->trace("MockCamacController::rgindex(n={},a={},f={}->{}) "
                "idx={}", n, a, f0, f, idx);
        return idx;
    }
};
using mock_camac_controller_ptr = std::shared_ptr<MockCamacController>;

/* Serializer/deserializer for MockCamacController::data_type
 */
NLOHMANN_JSON_NAMESPACE_BEGIN
template<>
struct adl_serializer<MockCamacController::data_type> {
    static void
    to_json(json &j, const MockCamacController::data_type &d) {
        j = json::array({d.mean(), d.stddev()});
    }
    static void
    from_json(const json &j, MockCamacController::data_type &d) {
        if (not (j.is_array() and j.size() >= 2))
            throw std::invalid_argument(
                    "json object is not a 2-element array");
        d = MockCamacController::data_type(
                j.at(0).get<float>(), j.at(1).get<float>());
    }
};
NLOHMANN_JSON_NAMESPACE_END


/**
 * KS3989 base class
 */
struct KS3989base {
    class Error : public std::error_category {
        public:
            enum values_e : int { noerr=0, nomatch, nosbe, numerrs };
            const char *name() const noexcept { return "ks3989_error"; }
            std::string message(int evalue) const {
                if (evalue < noerr or evalue >= numerrs)
                    throw std::out_of_range(fmt::format(
                                "{} - not a ks3989 error", evalue));
                return std::string(messages[evalue]);
            }
        private:
            static const char *messages[numerrs];
    };

    enum ascii_e : char { NUL=0, SOH=1, STX=2, ETX=3, SHFI=15, XON=17,
        XOFF=19, ESC=27 };
    enum csr_e : unsigned short  { NOQ=0x1, NOX=0x2, TC=0x4,
        ONLINE=0x8, I=0x10, SI=0x20, C=0x40, Z=0x80, BT=0x300,
        SBE=0x400, M=0x3800 };

    /**
     * Decomma's a string of formatted integers separated by commas
     * into a vector of ints.  Used by decode() to parse the naf
     * triplet and the data triplet.
     */ 
    static std::vector<int> decomma(const std::string&);

    /**
     * Converts the input string into a string of bytes, formatting
     * non-printing chars as hexadecimals.
     */
    static std::string repr(const std::string &t) {
        std::string s;
        for (const auto &c : t)
            if (isprint(c))
                s.append(1, c);
            else
                s.append(fmt::format("\\x{:02x}", int(c)));
        return s;
    }
};


class KS3989 : public std::recursive_timed_mutex, public KS3989base {
public:
    typedef int data_type;
    typedef std::variant<data_type, std::error_condition> ioget_type;

    struct StopParam { int n; int a; int f; data_type d; };

    KS3989(serial_ptr ser_, unsigned int port_=0, char cratenr_=1);
    std::string ccname() const { 
        return fmt::format("KS3989({},{})", ser->name(), port);
    }

    /**
     * Read NAF, ie f(0..7)
     */
    ioget_type get(int n, int a, int f, int msecs=-1);

    /**
     * Write and Control NAF, ie, f(8..31)
     */
    std::error_condition set(int n, int a, int f, const data_type &d,
            int msecs=-1);

    /**
     * @brief Call set() on each element of the argument.
     * @param[in] spa   vector of StopParam's
     *
     * This is an emergency stop function.  Instead of requesting each
     */
    bool stopall(const std::vector<StopParam>&);

    /**
     * Retrieves contents of control status register
     */
    ioget_type get_csr() { return naf(30, 0, 1, -1); }

    /**
     * Sets the control status register
     * @param bits Bits to set
     *
     * The mask enforces 24-bit single transfers and the SBE bit
     * enables a status byte with each response. The status byte
     * contains the Q and X so we don't have to read the CSR again to
     * determine them for the last NAF.
     */
    ioget_type set_csr(int bits) {
        int mask = ~(BT|M);
        return naf(30, 0, 17, -1, ((bits & mask)|SBE));
    }

    /**
     * Get SBE
     */
    unsigned char get_sbe() const { return sbe; }

    /* FIXME Temporary for testing.  Uncomment the private version below.
    ioget_type naf(int n, int a, int f, int msecs, int wrdata=0);
    */

    KS3989() = delete;
    KS3989(const KS3989&) = delete;
    KS3989& operator=(const KS3989&) = delete;

private:
    char cratenr;
    serial_ptr ser;
    unsigned int port;
    std::regex rspx;
    unsigned char sbe;

    ioget_type naf(int n, int a, int f, int msecs, int wrdata=0);
};
using ks3989_ptr = std::shared_ptr<KS3989>;


/* This is also a mockup CAMAC controller, but it is used only to test
 * template specialization.
 */
class M3989 : public std::recursive_timed_mutex, public KS3989base {
public:
    typedef int data_type;
    typedef std::variant<data_type, std::error_condition> ioget_type;

    explicit M3989(char cratenr_=1)
        : std::recursive_timed_mutex(), cratenr(cratenr_), sbe(0),
        logr(spdlog::get(LOGRNAME))
    {
        if (!logr) 
            throw std::runtime_error(fmt::format(
                        "{}/{}: cannot find logger \"{}\"",
                        __FILE__, __LINE__, LOGRNAME));
        logr->debug("M3989::M3989(cratenr={})@{}",
                cratenr_, fmt::ptr(this));
    }
    ioget_type get(int n, int a, int f, int msecs=-1);
    std::error_condition set(int n, int a, int f, const data_type &d,
            int msecs=-1);
    ioget_type get_csr() { return naf(30, 0, 1, -1); }
    ioget_type set_csr(int bits) {
        int mask = ~(BT|M);
        return naf(30, 0, 17, -1, ((bits & mask)|SBE));
    }
    unsigned short get_sbe() const { return sbe; }

    constexpr const char *ccname() const { return "M3989"; }
    M3989(const M3989&) = delete;
    M3989 &operator=(const M3989&) = delete;

private:
    char cratenr;
    unsigned short sbe;
    std::shared_ptr<spdlog::logger> logr;

    ioget_type naf(int n, int a, int f, int msecs, int wrdata=0);
};
using m3989_ptr = std::shared_ptr<M3989>;


/* for spdlog to log normal_distribution's param_type.
 */

template<>
struct fmt::formatter<std::normal_distribution<float>::param_type> {
    constexpr auto parse(format_parse_context &ctx) ->
    decltype(ctx.begin())
    {
        auto i = ctx.begin(), end = ctx.end();
        if (i != end) i++;
        if (i != end && *i != '}')
            throw format_error("invalid format");
        return i;
    }

    template<typename FormatContext>
    auto format(const std::normal_distribution<float>::param_type &ndp,
            FormatContext &ctx) ->
    decltype(ctx.out())
    {
        return fmt::format_to(ctx.out(), "({},{})", ndp.mean(),
                ndp.stddev());
    }
};
