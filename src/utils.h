/**
 * @file utils.h
 * @author Ken Teh
 * @date 20220615
 *
 * Assorted utilities.
 */
#ifndef utils_h
#define utils_h 1
#include <mqtt/string_collection.h>
#include <cstdio>
#include <toml.hpp>

namespace mqtt {

/**
 * @brief Join an mqtt string_collection
 * @param[in] sc    an mqtt string_collection
 * @return string vector
 */
inline std::vector<std::string>
vectorize_string_collection(const mqtt::string_collection &sc)
{
    std::vector<std::string> v;
    for (auto i = 0ul; i < sc.size(); ++i) v.push_back(sc[i]);
    return v;
}


/**
 * @brief Token type names
 */
extern const char *const tokentypes[];


/**
 * @brief Converts the toml mqtt table into a struct.
 */
struct ConnectData {
    int port;
    std::string broker;
    std::string brokerURI;
    bool secure;
    std::string cafile;
    std::string username;
    std::string userpasswd;

    void from_toml(const toml::value&);
};


}


namespace fmacp {

/**
 * @brief Strings for future_status enum
 */
extern const char *const future_status_labels[];

/**
 * @brief Compare string ending
 *
 * Available as a string member function in C++20
 */
bool endswith(const std::string &s, const char *ending);

/**
 * @brief Compare string beginning
 *
 * Available as a string member function in C++20
 */
bool startswith(const std::string &s, const char *starting);

/**
 * @brief Set logger file stream to auto flush lines.
 */
void lineflush(const std::string &fnm, std::FILE *logf);

/**
 * @brief Merge toml tables.
 */
void merge_config(const toml::table&, toml::table&);

/**
 * @brief Walk a toml table and saving each visited in a printable format.
 */
void tomlwalk(int nindent, const std::string &tblnm,
        const toml::table &tbl, std::vector<std::string> &walkout);

/**
 * @brief Replace posix environment variables in a string with their
 * values.
 */
std::string replace_envvars(const std::string&);
}


#endif
