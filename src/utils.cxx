/**
 * @file utils.cxx
 * @author Ken Teh
 * @date 20220615
 */
#include "utils.h"
#include <fmt/format.h>
#include <cstdlib>
#include <regex>

namespace mqtt {

const char *const tokentypes[] = { "CONNECT", "SUBSCRIBE", "PUBLISH",
    "UNSUBSCRIBE", "DISCONNECT" };

void ConnectData::from_toml(const toml::value &v)
{
    port = toml::find_or(v, "port", 0);
    broker = toml::find<std::string>(v, "broker");
    secure = toml::find<bool>(v, "secure");
    cafile = toml::find<std::string>(v, "cafile");
    username = toml::find<std::string>(v, "username");
    userpasswd = toml::find<std::string>(v, "userpasswd");

    /* Construct the broker URI per paho c++ mqtt convention.
     */
    if (secure) {
        if (port == 0) port = 8883;
        brokerURI = fmt::format("ssl://{}:{}", broker,  port);
    }
    else {
        if (port == 0) port = 1883;
        brokerURI = fmt::format("tcp://{}:{}", broker,  port);
    }
}

}

namespace fmacp {

const char *const future_status_labels[] = { "ready", "timeout",
    "deferred" };

bool endswith(const std::string &s, const char *t)
{
    std::string_view e(t);
    std::string se(s.end() - e.size(), s.end());
    return se == e;
}

bool startswith(const std::string &s, const char *t)
{
    std::string_view b(t);
    std::string sb(s.begin(), s.begin()+b.size());
    return sb == b;
}

void lineflush(const std::string &fnm, std::FILE *logf)
{
    // BUFSIZ (in stdio.h) is 8192.
    if (setvbuf(logf, nullptr, _IOLBF, BUFSIZ) != 0)
        throw std::runtime_error(fmt::format(
                    "set line buffering on {} failed", fnm));
}

static const std::regex rxenv(R"((\$\{*)([A-Z][A-Z0-9]*)(\}*))");
static const std::regex_constants::match_flag_type rxenvflags =
    std::regex_constants::match_default|
    std::regex_constants::format_first_only;

std::string replace_envvars(const std::string &s)
{
    std::smatch matches;
    if (!std::regex_search(s, matches, rxenv)) return s;
    std::string t;
    std::string vnm;
    char *val;
    vnm = matches[2].str();     // See rxenv for regex pattern.
    if ((val = getenv(vnm.c_str())) == nullptr) 
        throw std::runtime_error(fmt::format("{} - no such environment "
                    "variable", vnm));
    else
        t = std::regex_replace(s, rxenv, val, rxenvflags);

    return replace_envvars(t);
}

void tomlwalk(int indent, const std::string &tblnm,
        const toml::table &tbl, std::vector<std::string> &walkout)
{
    for (auto &kv : tbl) {
        if (kv.second.is_table()) {
            std::string subtblnm(tblnm);
            subtblnm = subtblnm.empty() ? kv.first :
                        subtblnm.append(1, '.').append(kv.first);
            fmacp::tomlwalk(++indent, subtblnm, kv.second.as_table(),
                    walkout);
        }
        else {
            std::string s;
            if (indent > 0) s.assign(indent, ' ');
            walkout.push_back(fmt::format("{}[{}] {}:{}", s, tblnm,
                        kv.first, toml::format(kv.second)));

        }
    }
}

void merge_config(const toml::table &ftbl, toml::table &ptbl)
{
    for (auto &f : ftbl) {
        if (f.second.is_table()) {
            /* Recursively call if f.second is a table AND ptbl also
             * contains the corresponding table.
             */
            try {
                auto &psub = ptbl.at(f.first);
                if (psub.is_table())
                    fmacp::merge_config(f.second.as_table(),
                            psub.as_table());
            }
            catch (std::exception &e) {;}
        }
        else {
            /* Else, overwrite ptbl's key/value with ftbl's.
             */
            try {
                auto &pv = ptbl.at(f.first);
                if (f.second.is_string())
                    pv = toml::value(replace_envvars(f.second.as_string()));
                else
                    pv = f.second;
            }
            catch (std::exception &e) {;}
        }
    }
}

}   // namespace fmacp
