/**
 * @file edipole.h
 * @author Ken Teh
 * @date 20221205
 */
#ifndef _edipole_h
#define _edipole_h 1
#include <mqtt/async_client.h>
#include <ktool/kthread.h>
#include <spdlog/spdlog.h>
#include <nlohmann/json.hpp>
#include <functional>
#include "poletrode.h"
#include "alistener.h"
#include "utils.h"
#include "fmacp.h"


/**
 * @brief Control/status thread for a CAMAC Electric Dipole.
 *
 * The template parameter is the CAMAC controller class.  Controls both
 * poles (ptrode, ntrode) of the electric dipole.
 */
template<typename T>
class CamacElectricDipole : private mqtt::async_client,
    public ktool::kthread<CamacElectricDipole<T>>
{
public:
    typedef T controller_type;
    typedef std::shared_ptr<T> controller_ptr;
    typedef typename controller_type::data_type data_type;
    typedef CamacPoleTrode<data_type> electrode_type;
    typedef std::unique_ptr<electrode_type> electrode_ptr;
    typedef typename electrode_type::camac_param_type camac_param_type;
    typedef typename electrode_type::ioret_type ioret_type;
    typedef typename electrode_type::values_type values_type;
    typedef ktool::kthread<CamacElectricDipole<T>> kthread_type;

    CamacElectricDipole(const char *nm_, electrode_ptr ptrode_,
            electrode_ptr ntrode_, controller_ptr cc_,
            const mqtt::ConnectData &mqttdata_);
    ~CamacElectricDipole();

    /* Some useful get functions.
     */
    std::string name() const { return nm; }
    bool is_mock() const { return mocked; }

    /* Control/status functions
     */
    void control(std::string&);
    void status();
    void setzero();
    void shutdown() {
        logr->info("CamacElectricDipole<T>::shutdown(), nm={}", nm);
        setzero();
    }

    /* Thread functions.
     */
    void start();
    void stopjoin();
    void threadfn();

    // Remove default/copy/assign.
    CamacElectricDipole() = delete;
    CamacElectricDipole(const CamacElectricDipole&) = delete;
    CamacElectricDipole &operator=(const CamacElectricDipole&) = delete;


private:
    std::string nm;
    electrode_ptr ptrode;
    electrode_ptr ntrode;
    controller_ptr cc;
    mqtt::ConnectData mqttdata;
    std::string settings_topic;
    std::string status_topic;
    AListener dcb;
    bool mocked;
    std::shared_ptr<spdlog::logger> logr;

    /* The mqtt callbacks.
     */
    void on_connected(const std::string&);
    void on_connection_lost(const std::string&);
    void on_message(mqtt::const_message_ptr);

};

template<typename T>
CamacElectricDipole<T>::CamacElectricDipole(const char *nm_,
        electrode_ptr ptrode_, electrode_ptr ntrode_, controller_ptr cc_,
        const mqtt::ConnectData &mqttdata_) :
    mqtt::async_client(mqttdata_.brokerURI, nm_, nullptr),
    ktool::kthread<CamacElectricDipole<T>>(nm_), nm(nm_),
    ptrode(std::move(ptrode_)), ntrode(std::move(ntrode_)), cc(cc_),
    mqttdata(mqttdata_),
    settings_topic(fmt::format("fma/{}/settings", nm_)),
    status_topic(fmt::format("fma/{}/status", nm_)),
    dcb(nm_),
    mocked(std::is_same<data_type,
            MockCamacController::data_type>::value),
    logr(spdlog::get(LOGRNAME))
{
    using namespace std::placeholders;

    if (!logr)
        throw std::runtime_error(fmt::format(
                    "{}/{}: cannot find logger \"{}\"",
                    __FILE__, __LINE__, LOGRNAME));
    logr->debug("CamacElectricDipole(nm=\"{}\",...)@{}",
            nm, fmt::ptr(this));

    auto fconnected = std::bind(&CamacElectricDipole<T>::on_connected,
                        this, _1);
    set_connected_handler(fconnected);
    auto fconnection_lost = std::bind(
            &CamacElectricDipole<T>::on_connection_lost, this, _1);
    set_connection_lost_handler(fconnection_lost);
    auto fmessage = std::bind(&CamacElectricDipole<T>::on_message,
                        this, _1);
    set_message_callback(fmessage);
}

template<typename T>
CamacElectricDipole<T>::~CamacElectricDipole()
{
    logr->debug("~CamacElectricDipole()@{}, nm={}",
            fmt::ptr(this), nm);
}


/**
 * @brief Write ssettings to the dipole's electrodes.
 * @param[in]   ssettings   json string with electrode settings
 *
 * ssettings is a json string with the following structure:
 * { "pos": { "vgoal": vgoal,...}, "neg": { "iprog": iprog, ... } }
 *
 * where the values vgoal, iprog, etc., are type T::data_type.
 *
 * This method is called from two places: (1) from on_message() when it
 * receives settings from the FMA GUI for this device, and (2) from
 * shutdown() which is called from the controlstatus thread.
 */
template<typename T>
void CamacElectricDipole<T>::control(std::string &ssettings)
{
    using namespace std::literals;
    using json = nlohmann::json;

    typename electrode_type::values_type psettings, nsettings;
    ioret_type prc, nrc;
    json jerr = R"({})"_json;
    std::string serr;
    bool timedout = false;

    logr->trace("CamacElectricDipole<T>::control(std::string &settings="
            "\"{}\"), nm={}", ssettings, nm);

    json jsettings = json::parse(ssettings);
    if (jsettings.find("pos") != jsettings.end())
        jsettings.at("pos").get_to(psettings);
    if (jsettings.find("neg") != jsettings.end())
        jsettings.at("neg").get_to(nsettings);

    /* I/O block to minimize locking the controller.
     */
    {
        std::unique_lock<controller_type> k(*cc, std::defer_lock);
        if (k.try_lock_for(CAMAC_LOCK_TIMEOUT)) {
            if (!psettings.empty()) prc = ptrode->control(psettings);
            if (!nsettings.empty()) nrc = ntrode->control(nsettings);
        }
        else {
            logr->warn("{} control lock {} controller timed out",
                    nm, cc->ccname());
            timedout = true;
        }
    }
    if (timedout) return;

    /* Publish any errors writing settings to device.
     */
    if (prc.second)
        jerr["error"]["pos"] = {{ prc.first, prc.second.message() }};
    if (nrc.second)
        jerr["error"]["neg"] = {{ nrc.first, nrc.second.message() }};
    if (!jerr.empty()) {
        serr = jerr.dump();
        try {
            /* Call publish() with a dcb since we don't  wait for publish
             * to complete.
             */
            publish(status_topic, serr.c_str(), serr.size(), 1, false, 
                    nullptr, dcb);
            logr->error("{} control errors, \"{}\"", nm, serr);
        }
        catch (const mqtt::exception &e) {
            logr->error("{} control publish \"{}\" exception - \"{}\"",
                    nm, serr, e.what());
        }
    }

    /* Clear ssettings to indicate we are done with it.
     */
    ssettings.clear();
}


/**
 * @brief Set dipole voltage and current to zero.
 */
template<typename T>
void CamacElectricDipole<T>::setzero()
{
    using json = nlohmann::json;

#ifdef MOCKED
    json jzero = R"({
        "pos": {
            "vgoal": [0.0, 0.1],
            "iprog": [0.0, 0.1]
        },
        "neg": {
            "vgoal": [0.0, 0.1],
            "iprog": [0.0, 0.1]
        }
    })"_json;
#else
    json jzero = R"({
        "pos": {
            "vgoal": 0.0,
            "iprog": 0.0
        },
        "neg": {
            "vgoal": 0.0,
            "iprog": 0.0
        }
    })"_json;
#endif
    auto ssettings = jzero.dump();
    logr->debug("CamacElectricDipole<T>::setzero(), nm={}, "
            "ssettings=\"{}\"", nm, ssettings);
    control(ssettings);
}

template<typename T>
void CamacElectricDipole<T>::status()
{
    using namespace std::literals;
    using json = nlohmann::json;

    logr->trace("CamacElectricDipole<T>::status(), nm={}", nm);

    values_type pmeas, nmeas;
    unsigned long msecs;
    ioret_type prc, nrc;
    bool timedout = false;

    {
        std::unique_lock<controller_type> k(*cc, std::defer_lock);
        if (k.try_lock_for(CAMAC_LOCK_TIMEOUT)) {
            pmeas = ptrode->status(prc);
            nmeas = ntrode->status(nrc);
            msecs = std::chrono::duration_cast<std::chrono::milliseconds>
                (std::chrono::system_clock::now().time_since_epoch())
                .count();
        }
        else {
            logr->warn("{} status lock {} controller timed out",
                    nm, cc->ccname());
            timedout = true;
        }
    }

    if (timedout) return;

    /* Convert pmeas, nmeas into json and publish it to the broker. If
     * there are errors, set qos=1 to guarantee delivery. Else, we fire and
     * forget.
     */
    json j = R"({})"_json;
    std::string s;
    int qos = (prc.second or nrc.second) ? 1 : 0;
    if (pmeas.empty())
        j["pos"] = nullptr;
    else
        j["pos"] = pmeas;
    if (nmeas.empty())
        j["neg"] = nullptr;
    else
        j["neg"] = nmeas;
    if (prc.second)
        j["error"]["pos"] = {{ prc.first, prc.second.message() }};
    if (nrc.second)
        j["error"]["neg"] = {{ nrc.first, nrc.second.message() }};
    j["msecs"] = msecs;     // status timestamp
    s = j.dump();

    try {
        /* Since we don't wait for the publish delivery, we call it with
         * the dcb callback. 
         */
        logr->debug("{} publish status \"{}\" to {}", nm, s, status_topic);
        publish(status_topic, s.c_str(), s.size(),
                qos, false, nullptr, dcb);
    }
    catch (const mqtt::exception &e) {
        logr->error("{} status publish \"{}\" exception - \"{}\"",
                nm, s, e.what());
    }
}

template<typename T>
void CamacElectricDipole<T>::on_connected(const std::string &s)
{
    logr->info("{} connected to {}", nm, mqttdata.brokerURI);
}

template<typename T>
void CamacElectricDipole<T>::on_connection_lost(const std::string &s)
{
    logr->warn("{} lost connection to {}", nm, mqttdata.brokerURI);
}

template<typename T>
void CamacElectricDipole<T>::on_message(mqtt::const_message_ptr m)
{
    auto ssettings = m->get_payload_str();
    logr->debug("CamacElectricDipole<T>::on_message: nm={}, "
            "payload={}", nm, ssettings);
    control(ssettings);
}


template<typename T>
void CamacElectricDipole<T>::start()
{
    using namespace std::chrono_literals;

    if (is_connected()) return;
    auto connect_opts = mqtt::connect_options_builder()
        .clean_session()
        .connect_timeout(BROKER_CONNECT_TIMEOUT)
        .finalize();

    std::string protocol = mqttdata.brokerURI.substr(0, 3);
    if (protocol == "ssl") {
        if (mqttdata.cafile.empty())
            throw std::runtime_error(fmt::format(
                        "{}/{}: no CA file for TLS connection",
                        __FILE__, __LINE__));
        auto ssl_opts = mqtt::ssl_options_builder()
            .trust_store(mqttdata.cafile)
            .enable_server_cert_auth(true)
            .verify(true)
            .finalize();
        connect_opts.set_ssl(ssl_opts);
        connect_opts.set_user_name(mqttdata.username);
        connect_opts.set_password(mqttdata.userpasswd);
    }

    /* Start thread if successfully connected to broker.
     */
    mqtt::token_ptr tok;
    tok = connect(connect_opts);
    if (!tok->wait_for(BROKER_CONNECT_TIMEOUT)) 
        throw std::runtime_error(fmt::format(
                    "{}/{}: connect to \"{}\" timed out",
                    __FILE__, __LINE__, mqttdata.brokerURI));
    kthread_type::start();
}

template<typename T>
void CamacElectricDipole<T>::stopjoin()
{
    /* Stop the thread, then disconnect from the broker.
     */
    kthread_type::stopjoin();
    if (is_connected()) {
        /* Notes:
         * - An mqtt action - connect, subscribe, etc - throws whether we
         *   wait on its token or not. It must be wrapped in a try/catch.
         * - Unlike start(), we don't let exceptions propagate out of
         *   stopjoin() because we need to join the thread.
         * - We don't wait on disconnect. IMPORTANT!! If we don't wait on
         *   an action, my convention is to call the action with a callback
         *   handler (here dal).
         */
        try {
            disconnect(nullptr, dcb);
        }
        catch (mqtt::exception &e) {
            logr->error("{} stopjoin() disconnect exception - \"{}\" <{}>",
                    nm, e.get_error_str(), e.get_return_code());
        }
    }
}

template<typename T>
void CamacElectricDipole<T>::threadfn()
{
    using namespace std::literals;

    logr->info("{} thread started", nm);

    /* The wait_for() method must be wrapped in a try/catch. It waits
     * on a condition var. If it times out, it returns false. If the
     * condition var is signaled, wait_for() then calls check_ret() to
     * check if the action succeeded or failed. If it succeeded, it returns
     * true, else it throws an exception.
     */
    try {
        if (!subscribe(settings_topic, 1)
                ->wait_for(BROKER_CONNECT_TIMEOUT)) {
            logr->error("{} subscribe {} timed out, thread exit...",
                    nm, settings_topic);
            kthread_type::exited = true;
            return;
        }
    }
    catch (mqtt::exception &e) {
        logr->error("{} subscribe {} failed - \"{}\" <{}>, thread exit...",
                nm, settings_topic, e.get_error_str(),
                e.get_return_code());
        kthread_type::exited = true;
        return;
    }
    logr->info("{} subscribed to {}", nm, settings_topic);

    // These are needed only if mocked is true.
    std::minstd_rand g;
    std::uniform_int_distribution<int> uid(MOCK_STATUS_LATENCY_MIN,
                                        MOCK_STATUS_LATENCY_MAX);
    std::chrono::milliseconds msecs;

    while (is_connected()) {
        /* DO NOT declare variables in this block unless they are 
         * re-assigned with each loop iteration.
         */
        if (kthread_type::stop_requested) {
            logr->info("{} stop requested", nm);
            break;
        }

        if (mocked) {
            /* Mock I/O latency reading device status.
             */
            msecs = std::chrono::milliseconds(uid(g));
            std::this_thread::sleep_for(msecs);
        }
        status();
    }

    try {
        shutdown();
        unsubscribe(settings_topic)->wait_for(BROKER_CONNECT_TIMEOUT);
    }
    catch (const mqtt::exception &e) {
        logr->error("{} unsubscribe {} failed - \"{}\" <{}>",
                nm, settings_topic, e.get_error_str(),
                e.get_return_code());
    }

    kthread_type::exited = true;
}

#endif
