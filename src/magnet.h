/**
 * @file magnet.h
 * @author Ken Teh
 * @date 20230105
 */
#ifndef _magnet_h
#define _magnet_h 1
#include <mqtt/async_client.h>
#include <ktool/kthread.h>
#include <spdlog/spdlog.h>
#include <nlohmann/json.hpp>
#include <functional>
#include "poletrode.h"
#include "alistener.h"
#include "utils.h"
#include "fmacp.h"


template<typename T>
class CamacMagnet : private mqtt::async_client,
    public ktool::kthread<CamacMagnet<T>>
{
public:
    typedef T controller_type;
    typedef std::shared_ptr<T> controller_ptr;
    typedef typename controller_type::data_type data_type;
    typedef CamacPoleTrode<data_type> pole_type;
    typedef std::unique_ptr<pole_type> pole_ptr;
    typedef typename pole_type::camac_param_type camac_param_type;
    typedef typename pole_type::ioret_type ioret_type;
    typedef typename pole_type::values_type values_type;
    typedef ktool::kthread<CamacMagnet<T>> kthread_type;

    CamacMagnet(const char *nm_, pole_ptr pole_,
            controller_ptr cc_, const mqtt::ConnectData &mqttdata_);
    ~CamacMagnet();

    /* Some useful get functions.
     */
    std::string name() const { return nm; }
    bool is_mock() const { return mocked; }

    /* Control/status functions
     */
    void control(std::string&);
    void status();
    void setzero();
    void shutdown() {
        logr->info("CamacMagnet<T>::shutdown(), nm={}", nm);
        setzero();
    }

    /* Thread functions.
     */
    void start();
    void stopjoin();
    void threadfn();

    // Remove default/copy/assign.
    CamacMagnet() = delete;
    CamacMagnet(const CamacMagnet&) = delete;
    CamacMagnet &operator=(const CamacMagnet&) = delete;


private:
    std::string nm;
    pole_ptr pole;
    controller_ptr cc;
    mqtt::ConnectData mqttdata;
    std::string settings_topic;
    std::string status_topic;
    AListener dcb;
    bool mocked;
    std::shared_ptr<spdlog::logger> logr;

    /* The mqtt callbacks.
     */
    void on_connected(const std::string&);
    void on_connection_lost(const std::string&);
    void on_message(mqtt::const_message_ptr);
};


template<typename T>
CamacMagnet<T>::CamacMagnet(const char *nm_, pole_ptr pole_,
        controller_ptr cc_, const mqtt::ConnectData &mqttdata_) :
    mqtt::async_client(mqttdata_.brokerURI, nm_, nullptr),
    ktool::kthread<CamacMagnet<T>>(nm_),
    nm(nm_), pole(std::move(pole_)), cc(cc_), mqttdata(mqttdata_),
    settings_topic(fmt::format("fma/{}/settings", nm_)),
    status_topic(fmt::format("fma/{}/status", nm_)), dcb(nm_), 
    mocked(std::is_same_v<data_type, MockCamacController::data_type>),
    logr(spdlog::get(LOGRNAME))
{
    using namespace std::placeholders;

    if (!logr)
        throw std::runtime_error(fmt::format(
                    "{}/{}: cannot find logger \"{}\"",
                    __FILE__, __LINE__, LOGRNAME));
    logr->debug("CamacMagnetDipole(nm=\"{}\",...)@{}", nm, fmt::ptr(this));

    auto fconnected = std::bind(&CamacMagnet<T>::on_connected, this, _1);
    set_connected_handler(fconnected);
    auto fconnection_lost = std::bind(
                            &CamacMagnet<T>::on_connection_lost, this, _1);
    set_connection_lost_handler(fconnection_lost);
    auto fmessage = std::bind(&CamacMagnet<T>::on_message, this, _1);
    set_message_callback(fmessage);
}

template<typename T>
CamacMagnet<T>::~CamacMagnet()
{
    logr->debug("~CamacMagnet()@{}, nm={}", fmt::ptr(this), nm);
}

template<typename T>
void CamacMagnet<T>::control(std::string &ssettings)
{
    using namespace std::chrono_literals;
    using json = nlohmann::json;

    logr->trace("CamacMagnet<T>::control(): ssettings=\"{}\", "
            "nm={}", ssettings, nm);

    typename pole_type::values_type psettings;
    ioret_type rc;
    bool timedout = false;
    json jsettings = json::parse(ssettings);
    jsettings.get_to(psettings);

    /* Mutex block for I/O.
     */
    {
        std::unique_lock<controller_type> k(*cc, std::defer_lock);
        if (k.try_lock_for(CAMAC_LOCK_TIMEOUT)) {
            rc = pole->control(psettings);
        }
        else {
            logr->warn("{} control lock {} controller timed out",
                    nm, cc->ccname());
            timedout = true;
        }
    }

    if (timedout) return;
    if (rc.second) {
        json jerr = {{ rc.first, rc.second.message() }};
        std::string serr = jerr.dump();
        try {
            /* Publish error with a dcb callback since we don't wait for
             * delivery to complete and also with qos=1 to guarantee
             * delivery.
             */
            publish(status_topic, serr.c_str(), serr.size(), 1, false,
                    nullptr, dcb);
            logr->error("{} control error - \"{}\"", nm, serr);
        }
        catch (const mqtt::exception &e) {
            logr->error("{} control publish \"{}\" exception - \"{}\"",
                    nm, serr, e.what());
        }
    }

    ssettings.clear();
}

/**
 * @brief Convenience method to set parameters to zero.
 */
template<typename T>
void CamacMagnet<T>::setzero()
{
    using json = nlohmann::json;

#ifdef MOCKED
    json jsettings = R"({
        "bgoal": [0.0, 0.05],
        "iprog": [0.0, 0.05],
        "cprog": [0.0, 0.05],
        "vprog": [0.0, 0.05]
    })"_json;
#else
    json jsettings = R"({
        "bgoal": 0.0,
        "iprog": 0.0
    })"_json;
#endif
    auto ssettings = jsettings.dump();
    logr->debug("CamacMagnet<T>::setzero(), nm={}, ssettings={}",
            nm, ssettings);
    control(ssettings);
}

template<typename T>
void CamacMagnet<T>::status()
{
    using namespace std::chrono_literals;
    using json = nlohmann::json;

    logr->trace("CamacMagnet<T>::status(), nm={}", nm);

    values_type meas;
    unsigned long msecs;
    ioret_type rc;
    bool timedout = false;

    {
        std::unique_lock<controller_type> k(*cc, std::defer_lock);
        if (k.try_lock_for(CAMAC_LOCK_TIMEOUT)) {
            meas = pole->status(rc);
            msecs = std::chrono::duration_cast<std::chrono::milliseconds>
                (std::chrono::system_clock::now().time_since_epoch())
                .count();
        }
        else {
            logr->warn("{} status lock {} controller timed out",
                    nm, cc->ccname());
            timedout = true;
        }
    }

    if (timedout) return;
    if (meas.empty()) {
        logr->warn("{} pole status is empty", nm);
        return;
    }

    /* Convert meas into a json and publish it. Set qos=1 if there are
     * errors to guarantee delivery, else fire and forget.
     */
    json j = R"({})"_json;
    int qos = 0;

    if (rc.second) {
        j["error"] = {{ rc.first, rc.second.message() }};
        qos = 1;
    }
    else {
        j = meas;
        j["msecs"] = msecs;
    }

    std::string dumped = j.dump();
    try {
        /* Publish with a dcb callback since we don't wait for publish
         * delivery.
         */
        logr->debug("{} publish status \"{}\" to {}", nm ,dumped,
                status_topic);
        publish(status_topic, dumped.c_str(), dumped.size(),
                qos, false, nullptr, dcb);
    }
    catch (const mqtt::exception &e) {
        logr->error("{} status publish \"{}\" exception - \"{}\"",
                nm, dumped, e.what());
    }
}

template<typename T>
void CamacMagnet<T>::on_connected(const std::string &s)
{
    logr->info("{} connected to {}", nm, mqttdata.brokerURI);
}

template<typename T>
void CamacMagnet<T>::on_connection_lost(const std::string &s)
{
    logr->warn("{} lost connection to {}", nm, mqttdata.brokerURI);
}

template<typename T>
void CamacMagnet<T>::on_message(mqtt::const_message_ptr m)
{
    auto ssettings = m->get_payload_str();
    logr->debug("CamacMagnet<T>::on_message: nm={}, "
            "payload={}", nm, ssettings);
    control(ssettings);
}

template<typename T>
void CamacMagnet<T>::start()
{
    using namespace std::chrono_literals;

    if (is_connected()) return;
    auto connect_opts = mqtt::connect_options_builder()
        .clean_session()
        .connect_timeout(BROKER_CONNECT_TIMEOUT)
        .finalize();

    std::string protocol = mqttdata.brokerURI.substr(0, 3);
    if (protocol == "ssl") {
        if (mqttdata.cafile.empty())
            throw std::runtime_error(fmt::format(
                        "{}/{}: no CA file for TLS connection",
                        __FILE__, __LINE__));
        auto ssl_opts = mqtt::ssl_options_builder()
            .trust_store(mqttdata.cafile)
            .enable_server_cert_auth(true)
            .verify(true)
            .finalize();
        connect_opts.set_ssl(ssl_opts);
        connect_opts.set_user_name(mqttdata.username);
        connect_opts.set_password(mqttdata.userpasswd);
    }

    /* Start thread if successfully connected to broker.
     */
    mqtt::token_ptr tok;
    tok = connect(connect_opts);
    if (!tok->wait_for(BROKER_CONNECT_TIMEOUT)) 
        throw std::runtime_error(fmt::format(
                    "{}/{}: connect to \"{}\" timed out",
                    __FILE__, __LINE__, mqttdata.brokerURI));
    kthread_type::start();
}

template<typename T>
void CamacMagnet<T>::stopjoin()
{
    /* Stop the thread, then disconnect from the broker.
     */
    kthread_type::stopjoin();
    if (is_connected()) {
        /* Notes:
         * - An mqtt action - connect, subscribe, etc - throws whether we
         *   wait on its token or not. It must be wrapped in a try/catch.
         * - Unlike start(), we don't let exceptions propagate out of
         *   stopjoin() because we need to join the thread.
         * - We don't wait on disconnect. IMPORTANT!! If we don't wait on
         *   an action, my convention is to call the action with a callback
         *   handler (here dal).
         */
        try {
            disconnect(nullptr, dcb);
        }
        catch (mqtt::exception &e) {
            logr->error("{} stopjoin() disconnect exception - \"{}\" <{}>",
                    nm, e.get_error_str(), e.get_return_code());
        }
    }
}

template<typename T>
void CamacMagnet<T>::threadfn()
{
    using namespace std::literals;

    logr->info("{} thread started", nm);

    /* The wait_for() method must be wrapped in a try/catch. It waits
     * on a condition var. If it times out, it returns false. If the
     * condition var is signaled, wait_for() then calls check_ret() to
     * check if the action succeeded or failed. If it succeeded, it returns
     * true, else it throws an exception.
     */
    try {
        if (!subscribe(settings_topic, 1)
                ->wait_for(BROKER_CONNECT_TIMEOUT)) {
            logr->error("{} subscribe {} timed out, thread exit...",
                    nm, settings_topic);
            kthread_type::exited = true;
            return;
        }
    }
    catch (mqtt::exception &e) {
        logr->error("{} subscribe {} failed - \"{}\" <{}>, thread exit...",
                nm, settings_topic, e.get_error_str(),
                e.get_return_code());
        kthread_type::exited = true;
        return;
    }
    logr->info("{} subscribed to {}", nm, settings_topic);

    // These are needed only if mocked is true.
    std::minstd_rand g;
    std::uniform_int_distribution<int> uid(MOCK_STATUS_LATENCY_MIN,
                                        MOCK_STATUS_LATENCY_MAX);
    std::chrono::milliseconds msecs;

    while (is_connected()) {
        /* DO NOT declare variables in this block unless they are 
         * re-assigned with each loop iteration.
         */
        if (kthread_type::stop_requested) {
            logr->info("{} stop requested", nm);
            break;
        }

        if (mocked) {
            /* Mock I/O latency reading device status.
             */
            msecs = std::chrono::milliseconds(uid(g));
            std::this_thread::sleep_for(msecs);
        }
        status();
    }

    try {
        shutdown();
        unsubscribe(settings_topic)->wait_for(BROKER_CONNECT_TIMEOUT);
    }
    catch (const mqtt::exception &e) {
        logr->error("{} unsubscribe {} failed - \"{}\" <{}>",
                nm, settings_topic, e.get_error_str(),
                e.get_return_code());
    }

    kthread_type::exited = true;
}
#endif
