/**
 * @file main.cxx
 * @author Ken Teh
 * @date 20231018
 */
#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/spdlog.h>
#include <ktool/sigpair.h>
#include <getopt.h>
#include <toml.hpp>
#include <filesystem>
#include "controlstatus.h"
#include "dtypes.h"
#include "fmacp.h"
#include "utils.h"

using namespace toml::literals;
const toml::value progg0 = R"(
    foreground = false
    verbose = 2             # spdlog's info level
    logfile = "fmacp.log"
    conffile = ""
    [mqtt]
    port = 0
    broker = "localhost"
    secure = false
    cafile = ""
    username = ""
    userpasswd = ""
)"_toml;
toml::value progg(progg0);

int loptn = 0;
struct option lopts[] = {
    { "foreground", no_argument, 0, 'f' },
    { "verbose", no_argument, 0, 'v' },
    { "logfile", required_argument, 0, 'L' },
    { "conffile", required_argument, 0, 'c' },
    { "broker", required_argument, 0, 'b' },
    { "port", required_argument, 0, 'p' },
    { "secure", no_argument, 0, 's' },
    { "cafile", required_argument, &loptn, 1 },
    { 0, 0, 0, 0 }
};
const char *sopts = "fvL:c:b:p:s";
std::shared_ptr<spdlog::logger> logr;

/* CAMAC controller, electric dipole electrodes, magnetic 2n-poles, etc.
 */
typename electric_dipole_type::controller_ptr cc;
typename electric_dipole_type::electrode_ptr ed1p;
typename electric_dipole_type::electrode_ptr ed1n;
typename electric_dipole_type::electrode_ptr ed2p;
typename electric_dipole_type::electrode_ptr ed2n;
typename camac_magnet_type::pole_ptr mdpole;
typename camac_magnet_type::pole_ptr q1pole;
typename camac_magnet_type::pole_ptr q2pole;
typename camac_magnet_type::pole_ptr q3pole;
typename camac_magnet_type::pole_ptr q4pole;

/* Organize devices into a container
 */
device_map_type devices;

/* The controlstatus thread.
 */
control_status_ptr controlstatus;

void kleenex(int signr)
{
    int x = signr < 0 ? 1 : 0;

    if (signr > 0) logr->info("signaled <{}>", signr);
    if (controlstatus) controlstatus->stopjoin();
    for (auto &d : devices) {
        switch (d.second.index()) {
            case 0:
                std::get<0>(d.second)->stopjoin();
                break;
            case 1:
                std::get<1>(d.second)->stopjoin();
                break;
        }
    }
    exit(x);
}

void make_devices(const mqtt::ConnectData &mqttdata)
{
    // CAMAC controller.
    cc = std::make_shared<electric_dipole_type::controller_type>();

    // ED1
    using electrode_param_type
        = electric_dipole_type::electrode_type::camac_param_type;
    using electrode_params_type
        = electric_dipole_type::electrode_type::params_type;

    ed1p = std::make_unique<electric_dipole_type::electrode_type>("ed1p",
            electrode_params_type({
                { "vgoal", electrode_param_type("vgoal", cc, 1, 0, 16,
                        nullptr, nullptr) },
                { "vprog", electrode_param_type("vprog", cc, 1, 0, 16,
                        nullptr, nullptr) },
                { "vmeas", electrode_param_type("vmeas", cc, 1, 0, 0,
                        nullptr, nullptr) },
                { "iprog", electrode_param_type("iprog", cc, 1, 1, 16,
                        nullptr, nullptr) },
                { "imeas", electrode_param_type("imeas", cc, 1, 1, 0,
                        nullptr, nullptr) },
            }), cc);
    ed1n = std::make_unique<electric_dipole_type::electrode_type>("ed1n",
            electrode_params_type({
                { "vgoal", electrode_param_type("vgoal", cc, 1, 2, 16,
                        nullptr, nullptr) },
                { "vprog", electrode_param_type("vprog", cc, 1, 2, 16,
                        nullptr, nullptr) },
                { "vmeas", electrode_param_type("vmeas", cc, 1, 2, 0,
                        nullptr, nullptr) },
                { "iprog", electrode_param_type("iprog", cc, 1, 3, 16,
                        nullptr, nullptr) },
                { "imeas", electrode_param_type("imeas", cc, 1, 3, 0,
                        nullptr, nullptr) },
            }), cc);
    devices.emplace("ed1", std::make_unique<electric_dipole_type>("ed1",
                std::move(ed1p), std::move(ed1n), cc, mqttdata));
    // get<0> retrieves the 0th variant type which is electric_dipole_ptr.
    // See device_type in dtypes.h.
    std::get<0>(devices["ed1"])->setzero();

    // ED2
    ed2p = std::make_unique<electric_dipole_type::electrode_type>("ed2p",
            electrode_params_type({
                { "vgoal", electrode_param_type("vgoal", cc, 1, 4, 16,
                        nullptr, nullptr) },
                { "vprog", electrode_param_type("vprog", cc, 1, 4, 16,
                        nullptr, nullptr) },
                { "vmeas", electrode_param_type("vmeas", cc, 1, 4, 0,
                        nullptr, nullptr) },
                { "iprog", electrode_param_type("iprog", cc, 1, 5, 16,
                        nullptr, nullptr) },
                { "imeas", electrode_param_type("imeas", cc, 1, 5, 0,
                        nullptr, nullptr) },
            }), cc);
    ed2n = std::make_unique<electric_dipole_type::electrode_type>("ed2n",
            electrode_params_type({
                { "vgoal", electrode_param_type("vgoal", cc, 1, 6, 16,
                        nullptr, nullptr) },
                { "vprog", electrode_param_type("vprog", cc, 1, 6, 16,
                        nullptr, nullptr) },
                { "vmeas", electrode_param_type("vmeas", cc, 1, 6, 0,
                        nullptr, nullptr) },
                { "iprog", electrode_param_type("iprog", cc, 1, 7, 16,
                        nullptr, nullptr) },
                { "imeas", electrode_param_type("imeas", cc, 1, 7, 0,
                        nullptr, nullptr) },
            }), cc);
    devices.emplace("ed2", std::make_unique<electric_dipole_type>("ed2",
                std::move(ed2p), std::move(ed2n), cc, mqttdata));
    std::get<0>(devices["ed2"])->setzero();

    // MD (magnetic dipole)
    using pole_param_type
        = camac_magnet_type::pole_type::camac_param_type;
    using pole_params_type
        = camac_magnet_type::pole_type::params_type;
    mdpole = std::make_unique<camac_magnet_type::pole_type>("mdpole",
            pole_params_type({
                { "bgoal", pole_param_type("bgoal", cc, 2, 0, 16,
                        nullptr, nullptr) },
                { "bmeas", pole_param_type("bmeas", cc, 2, 0, 0,
                        nullptr, nullptr) },
                { "iprog", pole_param_type("iprog", cc, 2, 1, 16,
                        nullptr, nullptr) },
                { "imeas", pole_param_type("imeas", cc, 2, 1, 0,
                        nullptr, nullptr) },
#ifdef MOCKED
                // There is no cprog or vprog except in mock mode.
                { "cprog", pole_param_type("cprog", cc, 2, 2, 16,
                        nullptr, nullptr) },
                { "vprog", pole_param_type("vprog", cc, 2, 3, 16,
                        nullptr, nullptr) },
#endif
                { "cmeas", pole_param_type("cmeas", cc, 2, 2, 0,
                        nullptr, nullptr) },
                { "vmeas", pole_param_type("vmeas", cc, 2, 3, 0,
                        nullptr, nullptr) }
            }), cc);
    devices.emplace("md", std::make_unique<camac_magnet_type>("md",
                std::move(mdpole), cc, mqttdata));
    // get<1> retrieves the 1st variant type which is camac_magnet_ptr.
    // See device_type in dtypes.h.
    std::get<1>(devices["md"])->setzero();

    // Q1 
    using pole_param_type
        = camac_magnet_type::pole_type::camac_param_type;
    using pole_params_type
        = camac_magnet_type::pole_type::params_type;
    q1pole = std::make_unique<camac_magnet_type::pole_type>("q1pole",
            pole_params_type({
                { "bgoal", pole_param_type("bgoal", cc, 3, 0, 16,
                        nullptr, nullptr) },
                { "bmeas", pole_param_type("bmeas", cc, 3, 0, 0,
                        nullptr, nullptr) },
                { "iprog", pole_param_type("iprog", cc, 3, 1, 16,
                        nullptr, nullptr) },
                { "imeas", pole_param_type("imeas", cc, 3, 1, 0,
                        nullptr, nullptr) },
#ifdef MOCKED
                // There is no cprog or vprog except in mock mode.
                { "cprog", pole_param_type("cprog", cc, 3, 2, 16,
                        nullptr, nullptr) },
                { "vprog", pole_param_type("vprog", cc, 3, 3, 16,
                        nullptr, nullptr) },
#endif
                { "cmeas", pole_param_type("cmeas", cc, 3, 2, 0,
                        nullptr, nullptr) },
                { "vmeas", pole_param_type("vmeas", cc, 3, 3, 0,
                        nullptr, nullptr) }
            }), cc);
    devices.emplace("q1", std::make_unique<camac_magnet_type>("q1",
                std::move(q1pole), cc, mqttdata));
    std::get<1>(devices["q1"])->setzero();

    // Q2 
    using pole_param_type
        = camac_magnet_type::pole_type::camac_param_type;
    using pole_params_type
        = camac_magnet_type::pole_type::params_type;
    q2pole = std::make_unique<camac_magnet_type::pole_type>("q2pole",
            pole_params_type({
                { "bgoal", pole_param_type("bgoal", cc, 3, 4, 16,
                        nullptr, nullptr) },
                { "bmeas", pole_param_type("bmeas", cc, 3, 4, 0,
                        nullptr, nullptr) },
                { "iprog", pole_param_type("iprog", cc, 3, 5, 16,
                        nullptr, nullptr) },
                { "imeas", pole_param_type("imeas", cc, 3, 5, 0,
                        nullptr, nullptr) },
#ifdef MOCKED
                // There is no cprog or vprog except in mock mode.
                { "cprog", pole_param_type("cprog", cc, 3, 6, 16,
                        nullptr, nullptr) },
                { "vprog", pole_param_type("vprog", cc, 3, 7, 16,
                        nullptr, nullptr) },
#endif
                { "cmeas", pole_param_type("cmeas", cc, 3, 6, 0,
                        nullptr, nullptr) },
                { "vmeas", pole_param_type("vmeas", cc, 3, 7, 0,
                        nullptr, nullptr) }
            }), cc);
    devices.emplace("q2", std::make_unique<camac_magnet_type>("q2",
                std::move(q2pole), cc, mqttdata));
    std::get<1>(devices["q2"])->setzero();

    // Q3 
    using pole_param_type
        = camac_magnet_type::pole_type::camac_param_type;
    using pole_params_type
        = camac_magnet_type::pole_type::params_type;
    q3pole = std::make_unique<camac_magnet_type::pole_type>("q3pole",
            pole_params_type({
                { "bgoal", pole_param_type("bgoal", cc, 3, 8, 16,
                        nullptr, nullptr) },
                { "bmeas", pole_param_type("bmeas", cc, 3, 8, 0,
                        nullptr, nullptr) },
                { "iprog", pole_param_type("iprog", cc, 3, 9, 16,
                        nullptr, nullptr) },
                { "imeas", pole_param_type("imeas", cc, 3, 9, 0,
                        nullptr, nullptr) },
#ifdef MOCKED
                // There is no cprog or vprog except in mock mode.
                { "cprog", pole_param_type("cprog", cc, 3, 10, 16,
                        nullptr, nullptr) },
                { "vprog", pole_param_type("vprog", cc, 3, 11, 16,
                        nullptr, nullptr) },
#endif
                { "cmeas", pole_param_type("cmeas", cc, 3, 10, 0,
                        nullptr, nullptr) },
                { "vmeas", pole_param_type("vmeas", cc, 3, 11, 0,
                        nullptr, nullptr) }
            }), cc);
    devices.emplace("q3", std::make_unique<camac_magnet_type>("q3",
                std::move(q3pole), cc, mqttdata));
    std::get<1>(devices["q3"])->setzero();

    // Q4 
    using pole_param_type
        = camac_magnet_type::pole_type::camac_param_type;
    using pole_params_type
        = camac_magnet_type::pole_type::params_type;
    q4pole = std::make_unique<camac_magnet_type::pole_type>("q4pole",
            pole_params_type({
                { "bgoal", pole_param_type("bgoal", cc, 3, 12, 16,
                        nullptr, nullptr) },
                { "bmeas", pole_param_type("bmeas", cc, 3, 12, 0,
                        nullptr, nullptr) },
                { "iprog", pole_param_type("iprog", cc, 3, 13, 16,
                        nullptr, nullptr) },
                { "imeas", pole_param_type("imeas", cc, 3, 13, 0,
                        nullptr, nullptr) },
#ifdef MOCKED
                // There is no cprog or vprog except in mock mode.
                { "cprog", pole_param_type("cprog", cc, 3, 14, 16,
                        nullptr, nullptr) },
                { "vprog", pole_param_type("vprog", cc, 3, 15, 16,
                        nullptr, nullptr) },
#endif
                { "cmeas", pole_param_type("cmeas", cc, 3, 14, 0,
                        nullptr, nullptr) },
                { "vmeas", pole_param_type("vmeas", cc, 3, 15, 0,
                        nullptr, nullptr) }
            }), cc);
    devices.emplace("q4", std::make_unique<camac_magnet_type>("q4",
                std::move(q4pole), cc, mqttdata));
    std::get<1>(devices["q4"])->setzero();
}

void fmacpmain()
{
    using namespace std::chrono_literals;

    sigpair ss[] = {{ SIGINT, kleenex }, { SIGTERM, kleenex },
        { 0, 0 }};
    sigpairinstall(ss);

    /* Logging. We always have a logfile. 
     */
    auto logfnm = toml::find<std::string>(progg, "logfile");
    if (logfnm.empty()) 
        logfnm = toml::find<std::string>(progg0, "logfile");
    std::vector<spdlog::sink_ptr> sinks;
    spdlog::file_event_handlers fh;
    fh.after_open = fmacp::lineflush;
    auto f = std::make_shared<spdlog::sinks::basic_file_sink_mt>
                (logfnm.c_str(), true, fh);
    f->set_pattern("[%C%m%d.%H%M%S.%e <%L>] %v");
    sinks.push_back(f);

    /* If foreground, log to console as well.
     */
    bool foreground = toml::find<bool>(progg, "foreground");
    if (foreground) {
        auto console = std::make_shared<spdlog::sinks::stderr_sink_mt>();
        console->set_pattern("[%M%S.%e <%L>] %v");
        sinks.push_back(console);
    }
    logr = std::make_shared<spdlog::logger>(LOGRNAME, begin(sinks),
            end(sinks));
    spdlog::register_logger(logr);  // required if an spdlog factory
                                    // function was not used to create
                                    // a logger.
    auto verbose = toml::find<int>(progg, "verbose");
    if (verbose < 0) verbose = 0;
    logr->set_level(spdlog::level::level_enum(verbose));

    /* Start the control status thread.
     */
    mqtt::ConnectData mqttdata;
    mqttdata.from_toml(toml::find(progg, "mqtt"));

    make_devices(mqttdata);
    controlstatus = std::make_unique<ControlStatus>("controlstatus",
                                                    mqttdata, devices);
    controlstatus->start();
    for (auto d = devices.begin(); d != devices.end(); ++d) {
        switch (d->second.index()) {
            case 0:
                std::get<0>(d->second)->start();
                break;
            case 1:
                std::get<1>(d->second)->start();
                break;
            default:
                break;
        }
    }

    /* TODO: Use the socketpair trick here as well. Pass the write-end to
     * control status thread and the devices threads so they can signal
     * when they exit.
     */
    bool again = true;
    while (again) {
        std::this_thread::sleep_for(1700ms);
        if (controlstatus->has_exited()) {
            logr->info("controlstatus thread exited??");
            again = false;
            continue;
        }

        again = std::none_of(devices.begin(), devices.end(),
                [](auto &d) -> bool {
                    bool d_exited = false;
                    switch (d.second.index()) {
                        case 0:
                            d_exited = std::get<0>(d.second)->has_exited();
                            break;
                        case 1:
                            d_exited = std::get<1>(d.second)->has_exited();
                            break;
                    }
                    return d_exited;
                });
    }

    kleenex(0);
}

bool reprogg(const char *fnm)
{
    std::filesystem::path conffile(fnm);
    size_t fsz;
    extern toml::value progg;

    if (!(std::filesystem::exists(conffile)
                and (fsz = std::filesystem::file_size(conffile)) > 0))
        throw std::runtime_error(fmt::format("{} - toml config file"));
    auto fprogg = toml::parse(fnm);
    progg = progg0;
    fmacp::merge_config(fprogg.as_table(), progg.as_table());
    progg.as_table().at("conffile") = toml::string(fnm);
    return true;
}

int main(int argc, char *argv[])
{
    bool reprogged = false;
    int c;

    opterr = 0;
    while ((c = getopt_long(argc, argv, sopts, lopts, 0)) != -1)
        switch (c) {
            case 'f':
                progg.as_table().at("foreground") = toml::boolean(true);
                break;
            case 'v':
                {
                    auto &verbose = progg.as_table().at("verbose")
                                        .as_integer();
                    verbose -= 1;
                }
                break;
            case 'L':
                progg.as_table().at("logfile") = toml::string(optarg);
                break;
            case 'c':
                if (!reprogged) {
                    reprogged = reprogg(optarg);
                    optind = 1;     // 1 to rescan argv.
                }
                break;
            case 'b':
                {
                    progg.as_table().at("mqtt").as_table()
                        .at("broker") = toml::string(optarg);
                }
                break;
            case 'p':
                {
                    progg.as_table().at("mqtt").as_table()
                        .at("port") = toml::integer(std::stoul(optarg));
                }
                break;
            case 's':
                {
                    progg.as_table().at("mqtt").as_table()
                        .at("secure") = toml::boolean(true);
                }
                break;
            case 0:
                switch (loptn) {
                    case 1:
                        {
                            progg.as_table().at("mqtt").as_table()
                                .at("cafile") = toml::string(optarg);
                        }
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }

    try {
        fmacpmain();
    }
    catch (std::exception &e) {
        if (logr)
            logr->error("{}", e.what());
        else
            fmt::print("{}\n", e.what());
    }

    kleenex(0);
    return 0;
}
