/**
 * @file camac.cxx
 * @author Ken Teh
 * @date 20211001
 */
#include <ktool/stringutils.h>
#include <algorithm>
#include <chrono>
#include "camac.h"


std::error_condition
MockCamacController::set(int n, int a, int f, const data_type &d, int msecs)
{
    std::error_condition noerr;

    /* Control f-codes are ignored. Read f-codes return an error.
     */
    if (0 <= f && f < 8)
        return std::error_condition(EINVAL, std::system_category());
    if ((8 <= f && f < 16) or (f >= 24)) return noerr;

    std::unique_lock<MockCamacController> k(*this, std::defer_lock);
    bool locked;

    locked = msecs < 0 ? k.lock(), true :
        k.try_lock_for(std::chrono::milliseconds(msecs));
    if (!locked)
        return std::error_condition(ETIMEDOUT, std::system_category());

    auto rgx = rgindex(f, n, a);
    bool has_rgx = (rg.find(rgx) != rg.end());
    data_type s;
    if (d.stddev() == 0.0) {
        /* d.stddev() is allowed to be 0.0 only if rg.at(rgx) is not empty.
         * This is used to change the rg[rgx] mean value but retain the
         * previous stddev.
         */
        if (!has_rgx) 
            return std::error_condition(EINVAL, std::system_category());
        else
            s = data_type(d.mean(), rg[rgx].stddev());
    }
    else {
        s = d;
    }

    /* Set the normal dist generator parameters.
     */
    rg[rgx].param(s);
    logr->trace("MockCamacController::set({},{},{},{},{})",
            n, a, f, s, msecs);
    return noerr;
}

bool MockCamacController::stopall(
        const std::vector<MockCamacController::StopParam> &spa)
{
    using namespace std::literals;
    std::unique_lock<MockCamacController> k(*this, std::defer_lock);

    logr->critical("MockCamacController::stopall()");
    if (!k.try_lock_for(CAMAC_STOP_TIMEOUT)) {
        logr->critical("MockCamacController::stopall(), "
                "failed to lock controller");
        return false;
    }

    int nerr = 0;
    for (auto const &sp : spa) {
        std::error_condition ec;
        logr->critical("MockCamacController::stopall(): "
                "set({},{},{},{})", sp.n, sp.a, sp.f, sp.d);
        ec = set(sp.n, sp.a, sp.f, sp.d);
        if (ec) {
            ++nerr;
            logr->error("MockCamacController::stopall(), "
                    "set({},{},{},{}) error",
                    sp.n, sp.a, sp.f, sp.d);
        }
    }

    return nerr > 0 ? false : true;
}

MockCamacController::ioget_type
MockCamacController::get(int n, int a, int f, int msecs)
{
    if (!(0 <= f && f < 8))
        return std::error_condition(EINVAL, std::system_category());

    std::unique_lock<MockCamacController> k(*this, std::defer_lock);
    bool locked;

    locked = msecs < 0 ? k.lock(), true :
        k.try_lock_for(std::chrono::milliseconds(msecs));
    if (!locked)
        return std::error_condition(ETIMEDOUT, std::system_category());

    auto rgx = rgindex(f, n, a);
    if (rg.find(rgx) == rg.end())
        return std::error_condition(EINVAL, std::system_category());
    auto sd = rg[rgx].stddev();
    data_type ret(rg[rgx](g), sd);
    logr->trace("MockCamacController::get({},{},{},{}), "
            "rgx={}, ret={}", n, a, f, msecs, rgx, ret.mean());
    return ret;
}


const char *KS3989base::Error::messages[] = {
    "no error",
    "bad naf response, no match",
    "bad naf response, no status byte"
};

std::vector<int> KS3989base::decomma(const std::string &s)
{
    auto sa = ktool::stringutils::split(s, ",");
    std::vector<int> v(sa.size());
    std::transform(sa.begin(), sa.end(), v.begin(),
            [](const auto &x) -> int { return std::stoi(x); });
    return v;
}

KS3989::KS3989(serial_ptr ser_, unsigned int port_, char cratenr_)
    : cratenr(cratenr_), ser(ser_), port(port_), rspx(), sbe()
{
    rspx.assign(R"(\x01(.)\x02([^\x03]*)\x03(.*))");

    auto rc = set_csr(KS3989::Z);
    if (std::holds_alternative<std::error_condition>(rc))
        throw std::runtime_error("KS3989 Z error");
    auto rc0 = std::get<int>(rc);
    if ((rc0 & KS3989::I) != 0)
        throw std::runtime_error("KS3989 I error");
    rc = naf(30, 0, 17, -1, rc0|KS3989base::SBE);
    if (std::holds_alternative<std::error_condition>(rc))
        throw std::runtime_error("KS3989 set SBE error");
}

KS3989::ioget_type KS3989::naf(int n, int a, int f, int msecs, int w)
{
    if (not ((1 <= n && n < 32) and (0 <= a and a < 16)
                and (0 <= f and f < 32)))
        return std::error_condition(EINVAL, std::system_category());

    std::unique_lock<KS3989> k(*this, std::defer_lock);
    bool locked;

    locked = msecs < 0 ? k.lock(), true :
                k.try_lock_for(std::chrono::milliseconds(msecs));
    if (!locked)
        return std::error_condition(ETIMEDOUT, std::system_category());

    /* Encode the NAF message.
     */
    std::string m;
    m.assign(1, SOH).append(1, cratenr)
        .append(fmt::format("{},{},{}", n, a, f))
        .append(1, STX);
    if (0 <= f and f < 8)
        m.append(1, ETX);
    else if (16 <= f and f < 24)
        m.append(fmt::format("{},{},{}", (w & 0x00ff0000) >> 16,
                    (w & 0x00ff00) >> 8, w & 0x00ff))
            .append(1, ETX);
    else
        ;

    /* Select the serial port and issue the naf. TODO Consider locking
     * serial device for all 3 calls: report, write, readall.
     */
    ser->report(port);
    auto wret = ser->write(m.c_str(), m.length());
    if (bool(wret.second)) return wret.second;

    /* Decode the response.
     */
    auto rsp = ser->readall();
    if (bool(rsp.second)) return rsp.second;

    std::smatch mrsp;
    if (!std::regex_match(rsp.first, mrsp, rspx))
        return std::error_condition(KS3989base::Error::nomatch,
                    KS3989base::Error());

    /* Since we are running with SBE set, we will always have a response
     * text even for writes and controls.
     */
    if (mrsp[2].str().length() == 0)
        return std::error_condition(KS3989base::Error::nosbe,
                    KS3989base::Error());

    auto da = decomma(mrsp[2].str());
    int ret = 0xff000000;
    if (0 <= f and f < 8) {
        ret = (da[0] << 16) | (da[1] << 8) | da[2];
        sbe = static_cast<unsigned char>(da[3] & 0x00ff);
    }
    else {
        sbe = static_cast<unsigned char>(da[0] & 0x00ff);
    }

    return ret;
}

KS3989::ioget_type KS3989::get(int n, int a, int f, int msecs)
{
    std::error_condition noerr;

    if (!(0 <=f and f < 8))
        return std::error_condition(EINVAL, std::system_category());
    return naf(n, a, f, 0, msecs);
}

std::error_condition KS3989::set(int n, int a, int f,
        const KS3989::data_type &d, int msecs)
{
    std::error_condition noerr;
    ioget_type rc;

    if (!(8 <= f and f < 32))
        return std::error_condition(EINVAL, std::system_category());
    rc = naf(n, a, f, msecs, d);
    return std::holds_alternative<std::error_condition>(rc) ? 
        std::get<std::error_condition>(rc) : noerr;
}


M3989::ioget_type
M3989::get(int n, int a, int f, int msecs)
{
    if (!(0 <= f && f < 8))
        return std::error_condition(EINVAL, std::system_category());

    std::unique_lock<M3989> k(*this, std::defer_lock);
    bool locked;

    locked = msecs < 0 ? k.lock(), true :
        k.try_lock_for(std::chrono::milliseconds(msecs));
    if (!locked)
        return std::error_condition(ETIMEDOUT, std::system_category());

    logr->debug("M3989::get({},{},{},{})", n, a, f, msecs);
    return naf(n, a, f, msecs);
}

std::error_condition
M3989::set(int n, int a, int f, const data_type &d, int msecs)
{
    if (!(8 < f && f < 32))
        return std::error_condition(EINVAL, std::system_category());

    std::unique_lock<M3989> k(*this, std::defer_lock);
    bool locked;

    locked = msecs < 0 ? k.lock(), true :
        k.try_lock_for(std::chrono::milliseconds(msecs));
    if (!locked)
        return std::error_condition(ETIMEDOUT, std::system_category());

    logr->debug("M3989::set({},{},{},{},{})", n, a, f, d, msecs);
    std::error_condition noerr;
    auto ret = naf(n, a, f, msecs, d);
    return (std::holds_alternative<std::error_condition>(ret)) ?
        std::get<1>(ret) : noerr;
}

M3989::ioget_type
M3989::naf(int n, int a, int f, int msecs, int wrdata)
{
    if (!((1 <= n && n < 32) && (0 <= a && a < 16)
                && (0 <= f && f < 32)))
        return std::error_condition(EINVAL, std::system_category());

    std::unique_lock<M3989> k(*this, std::defer_lock);
    bool locked;

    locked = msecs < 0 ? k.lock(), true :
        k.try_lock_for(std::chrono::milliseconds(msecs));
    if (!locked)
        return std::error_condition(ETIMEDOUT, std::system_category());

    logr->debug("M3989::naf({},{},{}, wrdata=0x{:08x})",
            n, a, f, wrdata);

    data_type ret = 0;
    if (n < 24) {
        ret = (0 <= f and f < 8) ? (a + ((n-1) << 4) + (f << 9)) : cratenr;
    }
    else {
        if (0 <= f and f < 8)
            ret = static_cast<int>(sbe);
        else if (16 <= f and f < 24)
            sbe = static_cast<unsigned short>(0x0000ffff & wrdata);
        else
            ret = 0;
            ;
    }
    return ret;
}
