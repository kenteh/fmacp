/**
 * @file controlstatus.h
 * @author Ken Teh
 * @date 20231018
 */
#ifndef _controlstatus_h
#define _controlstatus_h
#include <mqtt/async_client.h>
#include <ktool/kthread.h>
#include <spdlog/spdlog.h>
#include <memory>
#include <nlohmann/json.hpp>
#include "dtypes.h"
#include "utils.h"

/**
 * @brief Control/status command message struct.
 */
struct CScommand {
    std::string cmd;
    std::string params;
};


/**
 * @brief Converts a json object into a CScommand struct.
 *
 * If it fails to convert, it returns a CScommand where cmd and params are
 * empty strings.
 */
void from_json(const nlohmann::json &j, CScommand &cscmd);


/**
 * @brief Control/status thread for the fmacp program.
 */
class ControlStatus : private mqtt::async_client,
    public ktool::kthread<ControlStatus>
{
public:
    typedef ktool::kthread<ControlStatus> kthread_type;

    /**
     * @brief ControlStatus constructor
     * @param[in] nm_           instance name
     * @param[in] mqttdata_     mqtt connect data
     * @param[in] devices_      devices: dipoles, quads, etc.
     */
    ControlStatus(const char *nm_, const mqtt::ConnectData &mqttdata_,
            device_map_type &devices_);
    ~ControlStatus() {
        if (rsk != -1) close(rsk);
        if (wsk != -1) close(wsk);
    }

    /* Thread functions
     */
    void start();
    void stopjoin();
    void threadfn();

private:
    std::string nm;
    mqtt::ConnectData mqttdata;
    device_map_type &devices;
    int rsk;
    int wsk;
    std::shared_ptr<spdlog::logger> logr;

    /* The mqtt handlers
     */
    void on_connected(const std::string&);
    void on_connection_lost(const std::string&);
    void on_message(mqtt::const_message_ptr);
};
using control_status_ptr = std::unique_ptr<ControlStatus>;

#endif
