/**
 * @author Ken Teh
 * @date 20220218
 */
#include "blackbox.h"
#include <chrono>
#include <thread>
#include <fmt/format.h>


unsigned int Blackbox::report(unsigned int port_)
{
    using namespace std::chrono_literals;
    unsigned int oldport;

    if (!(0 <= port_ and port_ < numports))
        throw std::range_error("bad port number");

    /* Changing the blackbox's I/O port is heuristic.
     */
    wret_type wret;
    std::string w;
    w.assign(1, '\x04').append(fmt::format("{}", port_));
    wret = write(w.c_str(), w.length());
    std::this_thread::sleep_for(50ms);
    oldport = port;
    port = port_;
    return oldport;
}

