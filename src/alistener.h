/**
 * @file alistener.h
 * @author Ken Teh
 * @date 20230224
 */
#ifndef _alistener_h
#define _alistener_h 1
#include <mqtt/iaction_listener.h>
#include <spdlog/spdlog.h>
#include <mqtt/token.h>
#include "fmacp.h"

class AListener : public virtual mqtt::iaction_listener
{
public:
    AListener() :
        mqtt::iaction_listener(), nm("unnamed listener"),
        logr(spdlog::get(LOGRNAME))
    {
        if (!logr) throw std::runtime_error(fmt::format(
                    "{}/{}: cannot find logger \"{}\"",
                    __FILE__, __LINE__, LOGRNAME));
    }
    AListener(const char *nm_) :
        mqtt::iaction_listener(), nm(nm_),
        logr(spdlog::get(LOGRNAME))
    {
        if (!logr) throw std::runtime_error(fmt::format(
                    "{}/{}: cannot find logger \"{}\"",
                    __FILE__, __LINE__, LOGRNAME));
    }

    void on_success(const mqtt::token&) override;
    void on_failure(const mqtt::token&) override;

private:
    std::string nm;
    std::shared_ptr<spdlog::logger> logr;

    std::string token_details(const mqtt::token&);
};

#endif
