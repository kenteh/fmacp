'''Assorted utilites
'''
import argparse
import defaults
import functools
import logging
import os
import paho.mqtt.client as mqtt
import re
import threading

__author__ = "Ken Teh"
__all__ = [ 'printf_re', 'replace_envvars', 'merge_config',
            'recursive_update_dict', 'tomlwalk', 'add_log_trace',
            'VerboseAction', 'MqttClient', 'Kthread' ]


# Regex to find format specifiers in a Python string
# From https://gist.github.com/rtnpro/3552019
field_name = '(?P<field_name>(?P<arg_name>\w+|\d+){0,1}'\
                '(?:(?P<attribute_name>\.\w+)|'\
                '(?P<element_index>\[(?:\d+|(?:[^\]]+))\]))*)'
conversion = '(?P<conversion>r|s)'
align = '(?:(?P<fill>[^}{]?)(?P<align>[<>^=]))'
sign = '(?P<sign>[\+\- ])'
width = '(?P<width>\d+)'
precision = '(?P<precision>\d+)'
type_ = '(?P<type_>[bcdeEfFgGnosxX%])'
format_spec = ''\
    '(?P<format_spec>'\
        '%(align)s{0,1}'\
        '%(sign)s{0,1}#?0?'\
        '%(width)s{0,1},?'\
        '(?:\.%(precision)s){0,1}'\
        '%(type)s{0,1}'\
    ')' % {
        'align': align,
        'sign': sign,
        'width': width,
        'precision': precision,
        'type': type_
}
replacement_field = ''\
    '\{'\
    '(?:'\
        '%(field_name)s{0,1}'\
        '(?:!%(conversion)s){0,1}'\
        '(?:\:%(format_spec)s){0,1}'\
    ')'\
    '\}' % {
        'field_name': field_name,
        'conversion': conversion,
        'format_spec': format_spec
}

printf_re = re.compile(
    '(?:' + replacement_field + '|'
        '%((?:(?P<ord>\d+)\$|\((?P<key>\w+)\))?(?P<fullvar>[+#-]*(?:\d+)?'
            '(?:\.\d+)?(hh\|h\|l\|ll)?(?P<type>[\w%])))'
    ')'
)


def replace_envvars(s):
    '''Replace posix environment variables in a string with their values.
    '''
    envpatt = r"(\$\{*)([A-Z][A-Z0-9]*)(\}*)"
    m = re.search(envpatt, s)
    if m is None: return s
    vnm = m.group(2)
    val = os.getenv(vnm)
    if val is None:
        raise RuntimeError(f"{vnm} - no such environment "
                "variable")
    else:
        t = re.sub(envpatt, val, s, count=1)

    return replace_envvars(t)

def recursive_update_dict(u, d):
    '''Recursively update u into d; u and d are dicts.'''
    if any(map(lambda x: isinstance(x, dict) is False, (u, d))):
        return d
    for k, v in d.items():
        if k not in u: continue     # ignore keys in u not in d
        if isinstance(v, dict):
            # NOTE: merge_args() and recursive_update_dict() could be
            # merged by testing if u is a vars(args). We'd need a 3rd
            # argument flag to indicate this.
            if isinstance(u[k], dict):
                d[k] = recursive_update_dict(u[k], v)
        else:
            d[k] = u[k]

        # Include expanding possible environment variables.
        if isinstance(d[k], str):
            d[k] = replace_envvars(d[k])

    return d

def merge_args(a, g):
    '''Merge argparse args into g.

    This is almost the same as recursive_update_dict() except args is
    non-hierarchical so it is always passed as is when this function is
    called recursively.'''
    vargs = vars(a) if isinstance(a, argparse.Namespace) else a
    if isinstance(vargs, dict) is False: return g
    for k, v in g.items():
        if isinstance(v, dict):
            g[k] = merge_args(vargs, v)
        else:
            if k in vargs and vargs[k] is not None:
                g[k] = vargs[k]
    return g

def merge_config(u, g):
    '''Merge program config by updating g with u.'''
    if isinstance(u, argparse.Namespace):
        g = merge_args(u, g)
    elif isinstance(u, dict):
        g = recursive_update_dict(u, g)
    else:
        raise TypeError("bad arguments' type")


def tomlwalk(tcnt, tblnm, tbl, walkout=[]):
    '''Recursively walk a toml table and append the key/value pair
    formatted to the walkout list.  Returns walkout. tbl is actually a dict
    since tomllib converts a toml file into a dict.'''
    for k, v in tbl.items():
        if isinstance(v, dict):
            walkout = tomlwalk(tcnt+1, k, v, walkout)
        else:
            indent = ' '*tcnt if tcnt > 0 else ''
            walkout.append(f"{indent}[{tblnm}]{k}: {v}")
    return walkout

    
def add_log_trace(logger):
    '''Add a trace level to logging and a trace method to the given logger.
    '''
    if not hasattr(logging, "TRACE"):
        setattr(logging, "TRACE", 5)
        logging.addLevelName(5, "TRACE")
    if isinstance(logger, logging.Logger):
        setattr(logger, "trace",
            functools.partial(logger.log, logging.TRACE))


class VerboseAction(argparse.Action):
    '''argparse.add_argument() action handler for verbose option that links
    its value to the logging module's log levels. Use so:

    parser.add_argument("-v", nargs=0, default=logging.INFO,
        action=VerboseAction)
    '''
    def __call__(self, parser, namespace, values, option_string=None):
        verbose = getattr(namespace, self.dest)
        if verbose >= logging.INFO:
            verbose -= logging.DEBUG
        else:
            if not hasattr(logging, "TRACE"):
                setattr(logging, "TRACE", 5)
                logging.addLevelName(5, "TRACE")
            verbose = logging.TRACE
        setattr(namespace, self.dest, verbose)


class MqttClient(mqtt.Client):
    '''mqtt.Client sets all handlers to None. Sub-class it and  define
    handlers.
    '''
    def __init__(self, name, client_id="", logrname=defaults.LOGRNAME):
        mqtt.Client.__init__(self, client_id=client_id, clean_session=True)
        self.nm = name
        self.logr = logging.getLogger(logrname)

    def on_connect(self, client, userdata, flags, rc):
        self.logr.debug(f"{self.nm} on_connect(client={client._client_id},"
            f" userdata={userdata}, flags={flags}, rc={rc})")

    def on_subscribe(self, client, userdata, mid, granted_qos):
        self.logr.debug(f"{self.nm} on_subscribe(client="
            f"{client._client_id}, userdata={userdata}, mid={mid}, "
            f"granted_qos={granted_qos})")

    def on_message(self, client, userdata, message):
#        if hasattr(self.logr, 'trace'):
#            self.logr.trace(f"{self.nm} on_message(client="
#                f"{client._client_id}, userdata={userdata}, message: "
#                f"topic={message.topic}, payload={message.payload}")
        self.logr.debug(f"{self.nm} on_message(client="
            f"{client._client_id}, userdata={userdata}, "
            f"message=({message.topic}, {message.payload.decode()})")

    def on_publish(self, client, userdata, mid):
        if hasattr(self.logr, 'trace'):
            self.logr.trace(f"{self.nm} on_publish(client="
                f"{client._client_id}, userdata={userdata}, mid={mid})")
        
    def on_unsubscribe(self, client, userdata, mid):
        self.logr.debug(f"{self.nm} on_unsubscribe(client="
            f"{client._client_id}, userdata={userdata}, mid={mid})")
        
    def on_disconnect(self, client, userdata, rc):
        self.logr.debug(f"{self.nm} on_disconnect(client="
            f"{client._client_id}, userdata={userdata}, rc={rc})")


class Kthread(threading.Thread):
    '''My thread base class. Requires _threadfn be implemented.'''
    def __init__(self, target=None, name=None):
        if target is None: target = self._threadfn
        super().__init__(name=name, target=target)
        self.stop_requested = True

    def run(self):
        self.stop_requested = False
        super().run()

    def stop(self):
        self.stop_requested = True

    def _threadfn(self):
        raise NotImplementedError
