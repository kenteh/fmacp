'''Write settings to FMA devices.

The normal way is via the viewer program. However this can also be used as
a standalone program.
'''
from argparse import ArgumentParser
import json
import logging
import paho.mqtt.publish as mqttpublish
import re
import readline
import sys
import traceback
import utils

if sys.version_info[1] >= 11:
    import tomllib
else:
    import tomli as tomllib

progg = tomllib.loads('''
foreground = false
verbose = 2
logfile = "testviewer.log"
conffile = ""
[mqtt]
port = 0
broker = "localhost"
secure = false
cafile = ""
username = ""
userpasswd = ""
''')


def publish(topic, payload, qos=1):
    try:
        mqttcfg = progg['mqtt']
        port = mqttcfg['port']
        if mqttcfg['secure']:
            tls = { 'ca_certs': mqttcfg['cafile'] }
            auth = { 'username': mqttcfg['username'],
                        'password': mqttcfg['userpasswd'] }
            if port == 0: port = 8883
        else:
            tls, auth = None, None
            if port == 0: port = 1883

        logr.debug(f"topic={topic}, payload={payload}, qos={qos}, "
            f"hostname={mqttcfg['broker']}, port={port}, auth={auth}, "
            f"tls={tls}")
        mqttpublish.single(topic, payload=payload, qos=qos,
            hostname=mqttcfg['broker'], port=port, auth=auth, tls=tls)
    except:
        trb = traceback.format_exc()
        logr.error(trb)


def magnet(line, ma):
    '''Parse line into magnet settings and send to broker.'''
    if len(ma) != 1:
        logr.error(f"\"{line}\", ma={ma}, expecting len(ma)=1")
        return

    dev, rhs = ma[0]
    settings = {}
    rxs = re.compile(r"(bgoal|iprog|vprog|cprog)\s*=\s*([\-\.0-9]+)")
    sma = rxs.findall(rhs.strip())
    if args.mock:
        settings.update({ p: [float(v), 0.05] for p, v in sma })
    else:
        settings.update({ p: float(v) for p, v in sma })
    logr.debug(f"{dev} {settings}")
    try:
        publish(f"fma/{dev}/settings", json.dumps(settings))
    except:
        trb = traceback.format_exc()
        logr.error(trb)
    
def edipole(line, ma):
    '''Parse line into ed1/ed2 settings and send settings to broker.'''
    # Check for errors.
    errmsg = None
    if len(ma) != 2:
        errmsg = f"\"{line}\" - expecting 2 matches"
    elif ma[0][0] != ma[1][0]:
        errmsg = f"\"{line}\" - setting different dipoles not supported"
    elif ma[0][1] == ma[1][1]:
        errmsg = f"\"{line}\" - same polarity settings"
    else:
        pass

    if errmsg != None:
        logr.error(errmsg)
        return
    
    try:
        settings = {}
        rxs = re.compile(r"(vgoal|iprog)\s*=\s*([\-\.0-9]+)")
        for dev, pol, s in ma:
            s11 = s[1:-1]
            sma = rxs.findall(s11)
            if len(sma) == 0:
                logr.error(f"\"{s}\" - bad setting")
                return
            if args.mock:
                settings.update(
                    { pol: { p: [float(v), 0.1] for p, v in sma } })
            else:
                settings.update({ pol: { p: float(v) for p, v in sma } })
        logr.debug(f"{dev} {settings}")
        
        topic = f"fma/{dev}/settings"
        payload = json.dumps(settings)
        publish(topic, payload)
    except:
        trb = traceback.format_exc()
        logr.error(trb)

def shutdown(line, ma):
    topic = "fma/controlstatus"
    payload = json.dumps({'cmd': "shutdown"})
    publish(topic, payload)

    
def main():
    rxa = [
        (re.compile(r'shutdown'), shutdown),
        (re.compile(r'(ed1|ed2)\.(pos|neg)\s*=\s*(\(.+?\))'), edipole),
        (re.compile(r'(q1|q2|q3|q4|md)\s*=\s*(\(.+?\))'), magnet),
    ]
    try:
        while True:
            line = input('? ').strip()
            if len(line) == 0: continue
            for rx, f in rxa:
                matches = rx.findall(line)
                if len(matches) > 0:
                    f(line, matches)
                    break
                
    except (KeyboardInterrupt, EOFError):
        print()
    except:
        trb = traceback.format_exc()
        logr.error(trb)
    finally:
        pass

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-v', '--verbose', nargs=0, default=logging.INFO,
        action=utils.VerboseAction, help='verbose mode')
    parser.add_argument('-c', '--conffile', default=None,
        help='toml config file')
    parser.add_argument('-m', '--mock', action='store_true', default=False,
        help='devices are mock')
    parser.add_argument('-b', '--broker', default=None,
        help='mqtt broker hostname')
    parser.add_argument('-p', '--port', default=0, type=int,
        help="mqtt broker port")
    parser.add_argument('-s', '--secure', action='store_true',
        default=None, help="connect to mqtt broker via ssl")
    parser.add_argument("--cafile", default=None,
        help="CA certificate filename")
    args, unknown = parser.parse_known_args()


    if bool(args.conffile):
        with open(args.conffile, "rb") as finp:
            fprogg = tomllib.load(finp)
        utils.merge_config(fprogg, progg)
        progg['conffile'] = args.conffile
    utils.merge_config(args, progg)     # Merge args into progg.

    logr = logging.getLogger("logr")
    logcons = logging.StreamHandler()
    cfmtter = logging.Formatter(datefmt="%M%S",
                fmt="<%(levelname)1.1s> %(message)s")
    logcons.setFormatter(cfmtter)
    logr.addHandler(logcons)
    logr.setLevel(progg['verbose'])

    # TODO: Remove this so args.mock works correctly.
    if args.mock is False:
        args.mock = True
        logr.warning("setdevice is mock mode")

    main()
