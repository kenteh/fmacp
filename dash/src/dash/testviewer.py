from argparse import ArgumentParser
import defaults
import logging
import sys
import utils


if sys.version_info[1] >= 11:
    import tomllib
else:
    import tomli as tomllib

progg0 = tomllib.loads('''
foreground = false
verbose = 2
logfile = "testviewer.log"
conffile = ""
[mqtt]
port = 0
broker = "localhost"
secure = false
cafile = ""
username = ""
userpasswd = ""
''')
progg = dict(**progg0)



def main():
    mwa = [ MainWindow0 ]
    if args.mx >= len(mwa): return
    app = QApplication([])
    mw = mwa[args.mx](progg)
    mw.show()
    app.exec_()

    
if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-f', '--foreground', action='store_true',
        default=None, help="run in foreground mode")
    parser.add_argument('-v', '--verbose', nargs=0, default=logging.INFO,
        action=utils.VerboseAction, help='verbose mode')
    parser.add_argument('-L', '--logfile', default=None, help="logfilename")
    parser.add_argument('-c', '--conffile', default=None,
        help='toml config file')
    parser.add_argument('-b', '--broker', default=None,
        help='mqtt broker hostname')
    parser.add_argument('-p', '--port', default=0, type=int,
        help="mqtt broker port")
    parser.add_argument('-s', '--secure', action='store_true',
        default=None, help="connect to mqtt broker via ssl")
    parser.add_argument("--cafile", default=None,
        help="CA certificate filename")
    parser.add_argument("mx", nargs='?', type=int, default=0)
    args, unknown = parser.parse_known_args()

    if bool(args.conffile):
        with open(args.conffile, "rb") as finp:
            fprogg = tomllib.load(finp)
        utils.merge_config(fprogg, progg)
        progg['conffile'] = args.conffile
    utils.merge_config(args, progg)     # Merge args into progg.

    # Must have a logfile. 
    progg['logfile'] = args.logfile if bool(args.logfile) else \
                        progg0['logfile']
    logr = logging.getLogger(defaults.LOGRNAME)
    ffmtter = logging.Formatter(datefmt="%y%m%d.%H%M%S",
                fmt="[%(asctime)s.%(msecs)03d <%(levelname)1.1s>] "
                "%(message)s")
    logfile = logging.FileHandler(progg['logfile'], "w")
    logfile.setFormatter(ffmtter)
    logr.addHandler(logfile)
    if progg['foreground']:
        logcons = logging.StreamHandler()
        cfmtter = logging.Formatter(datefmt="%M%S",
                    fmt="[%(asctime)s.%(msecs)03d <%(levelname)1.1s>] "
                    "%(message)s")
        logcons.setFormatter(cfmtter)
        logr.addHandler(logcons)
    logr.setLevel(progg['verbose'])
    main()
