from PySide2.QtCore import Qt, Signal
from PySide2.QtGui import QDoubleValidator, QIntValidator
from PySide2.QtWidgets import QApplication, QMainWindow, QWidget,\
    QHBoxLayout, QVBoxLayout, QFrame, QLabel, QPushButton,\
    QLineEdit, QFormLayout
import argparse
import defaults
import logging

class CIForm(QFrame):
    def __init__(self, viewer, data_updated_signal=None):
        super().__init__()
        self.logr = logging.getLogger(defaults.LOGRNAME)
        self.viewer = viewer
        if isinstance(data_updated_signal, Signal):
            self.data_updated_signal = data_updated_signal
            self.data_updated_signal.connect(self.update_data)

        self.setObjectName("central_ion_form")
        self.setFrameShape(QFrame.Box)
        self.setFrameShadow(QFrame.Sunken)
        self.setLineWidth(1)

        layout = QVBoxLayout()
        title = QLabel()
        title.setText(title.tr("Central Particle Parameters"))
        title.setAlignment(Qt.AlignCenter)
        layout.addWidget(title)
        layout.addSpacing(10)

        self.parameters = {
            'energy': {
                'label': self.tr("Energy (MeV)"),
                'input': None,
                'validator': QDoubleValidator(),
                'fmt': '{:<6.2f}',
                'value': 0.0
            },
            'charge': {
                'label': self.tr("Charge"),
                'input': None,
                'validator': QIntValidator(),
                'fmt': '{:d}',
                'value': 0
            },
            'mass': {
                'label': self.tr('Mass (amu)'),
                'input': None,
                'validator': QIntValidator(),
                'fmt': '{:d}',
                'value': 0
            }
        }
        self.parameters['energy']['validator'].setBottom(0.0)
        self.parameters['charge']['validator'].setBottom(1)
        self.parameters['mass']['validator'].setBottom(1)
        form_layout = QFormLayout()
        for pnm, p in self.parameters.items():
            p['input'] = QLineEdit(self.tr(
                            p['fmt'].format(p['value']).strip()))
            p['input'].setReadOnly(True)
            form_layout.addRow(self.tr(p['label']), p['input'])
        layout.addLayout(form_layout)

        self.newbtn = QPushButton(self.tr("New"))
        self.newbtn.clicked.connect(self.newbtn_clicked)
        self.okbtn = QPushButton(self.tr("Ok"))
        self.okbtn.clicked.connect(self.okbtn_clicked)
        self.okbtn.setEnabled(False)
        self.cancelbtn = QPushButton(self.tr("Cancel"))
        self.cancelbtn.clicked.connect(self.cancelbtn_clicked)
        self.cancelbtn.setEnabled(False)

        buttons_layout = QHBoxLayout()
        buttons_layout.addWidget(self.newbtn)
        buttons_layout.addWidget(self.cancelbtn)
        buttons_layout.addWidget(self.okbtn)
        layout.addLayout(buttons_layout)

        self.setLayout(layout)

    def update_data(self):
        pass

    def newbtn_clicked(self, checked):
        self.newbtn.setEnabled(False)
        for pnm, p in self.parameters.items():
            p['input'].setReadOnly(False)
            if pnm == 'energy':
                p['input'].selectAll()

        self.okbtn.setEnabled(True)
        self.cancelbtn.setEnabled(True)

    def okbtn_clicked(self, checked):
        self.okbtn.setEnabled(False)
        self.cancelbtn.setEnabled(False)
        self.newbtn.setEnabled(True)
        ci = { 'form': 'ci' }
        for pnm, p in self.parameters.items():
            p['input'].deselect()
            p['value'] = float(p['input'].text()) if pnm == 'energy' \
                            else  int(p['input'].text())
            p['input'].setReadOnly(True)
            ci[pnm] = p['value']
        self.viewer.post("ci", ci)

    def cancelbtn_clicked(self, checked):
        self.okbtn.setEnabled(False)
        self.cancelbtn.setEnabled(False)
        self.newbtn.setEnabled(True)
        for pnm, p in self.parameters.items():
            p['input'].deselect()
            p['input'].setText(self.tr(p['fmt'].format(p['value']).strip()))
            p['input'].setReadOnly(True)
def ciform_main():
    app = QApplication([])
    mw = QMainWindow()

    cf = CIForm(mw)
    fw = QWidget(parent=mw)
    fw_layout = QHBoxLayout(fw)
    fw_layout.addWidget(cf)
    mw.setCentralWidget(fw)
    mw.show()
    app.exec_()

if __name__ == "__main__":
    mfuncs = [ ciform_main ]
    parser = argparse.ArgumentParser()
    parser.add_argument("mx", nargs='?', type=int, default=0)
    args, unknown = parser.parse_known_args()

    logging.basicConfig(datefmt="%M%S", format="[%(asctime)s] %(message)s")
    logr = logging.getLogger(defaults.LOGRNAME)

    if args.mx < len(mfuncs):
        mfuncs[args.mx]()
