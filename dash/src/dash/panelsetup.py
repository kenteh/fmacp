'''GUI panels and tables setup info.
'''

__all__ = [ 'qpanel_setup', 'edpanel_setup', 'tbl_setup' ]
__author__ = 'Ken Teh'

_bfmt = '{:>5.3f}'
_vifmt = '{:>4.1f}'
qpanel_setup = {
    'bgoal': { 'label': "Desired Tip Field:", 'fmt': _bfmt,
        'unit': 'T' },
    'bmeas': { 'label': "Measured Tip Field:", 'fmt': _bfmt,
        'unit': 'T', 'meter': (0.0, 1.0) },
    'iprog': { 'label': "Programmed Current:", 'fmt': _vifmt,
        'unit': 'A' },
    'imeas': { 'label': "Measured Current:", 'fmt': _vifmt,
        'unit': 'A', 'meter': (0.0, 400.) },
    'vmeas': { 'label': "Power Supply Voltage:", 'fmt': _vifmt,
        'unit': 'V' },
    'cmeas': { 'label': "Hall Probe Temperature:",
        'fmt': '{:>4.1f}', 'unit': '°C' }
}

_vfmt = '{:>6.2f}'
_ifmt = '{:>5.1f}'
edpanel_setup = {
    'vgoal': { 'label': 'Desired Voltage:', 'fmt': _vfmt,
        'unit': 'kV' },
    'vprog': { 'label': 'Programmed Voltage:', 'fmt': _vfmt,
        'unit': 'kV' },
    'vmeas': { 'label': 'Measured Voltage:', 'fmt': _vfmt,
        'unit': 'kV', 'meter': (0., 300.) },
    'iprog': { 'label': 'Programmed Current:', 'fmt': _ifmt,
        'unit': 'μA' },
    'imeas': { 'label': 'Measured Current:', 'fmt': _ifmt,
        'unit': 'μA', 'meter': (0., 120.) },
}

def tbl_setup(meas_or_calc):
    assert meas_or_calc in ('meas', 'calc'),\
        f"{meas_or_calc} - bad argument"
    suff = 'meas' if meas_or_calc == 'meas' else 'goal'
    return {
        'q1': { 'varnm': f'b{suff}', 'label': "Q1", 'fmt': '{:>6.4f}',
                'unit': 'T' },
        'q2': { 'varnm': f'b{suff}', 'label': "Q2", 'fmt': '{:>6.4f}',
                'unit': 'T' },
        'ed1.pos': { 'varnm': f'v{suff}', 'label': 'ED1+',
                    'fmt': '{:>6.2f}', 'unit': 'kV' },
        'ed1.neg': { 'varnm': f'v{suff}', 'label': 'ED1-',
                    'fmt': '{:>6.2f}', 'unit': 'kV' },
        'md': { 'varnm': f'b{suff}', 'label': "MD", 'fmt': '{:>7.5f}',
                'unit': 'T' },
        'ed2.pos': { 'varnm': f'v{suff}', 'label': 'ED2+',
                    'fmt': '{:>6.2f}', 'unit': 'kV' },
        'ed2.neg': { 'varnm': f'v{suff}', 'label': 'ED2-',
                    'fmt': '{:>6.2f}', 'unit': 'kV' },
        'q3': { 'varnm': f'b{suff}', 'label': "Q3", 'fmt': '{:>6.4f}',
                'unit': 'T' },
        'q4': { 'varnm': f'b{suff}', 'label': "Q4", 'fmt': '{:>6.4f}',
                'unit': 'T' },
    }
