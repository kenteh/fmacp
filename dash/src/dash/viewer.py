from PySide2.QtCore import QSocketNotifier, Signal
from PySide2.QtWidgets import QApplication, QMainWindow, QWidget,\
    QHBoxLayout, QVBoxLayout, QTabWidget
from argparse import ArgumentParser
from panelsetup import edpanel_setup, qpanel_setup, tbl_setup
import defaults
import fidgets
import json
import logging
import paho.mqtt.client as mqtt
import paho.mqtt.publish as mqttpublish
import sys
import utils


if sys.version_info[1] >= 11:
    import tomllib
else:
    import tomli as tomllib

progg0 = tomllib.loads('''
foreground = false
verbose = 2
logfile = "testviewer.log"
conffile = ""
[mqtt]
port = 0
broker = "localhost"
secure = false
cafile = ""
username = ""
userpasswd = ""
''')
progg = dict(**progg0)


class MainWindow(QMainWindow):
    status_updated = Signal(dict)

    def clnt_cleanup(self):
        if self.clnt_subscribed:
            self.clnt.unsubscribe(self.topics)
        if self.clnt.is_connected():
            self.crntf.setEnabled(False)
            self.cwntf.setEnabled(False)
            self.clnt.disconnect()

    def clnt_want_write(self):
        # Disable write notifier except when want_write() is true.
        if self.clnt.want_write():
            if not self.cwntf.isEnabled():
                self.cwntf.setEnabled(True)
        else:
            if self.cwntf.isEnabled():
                self.cwntf.setEnabled(False)

    def clnt_read_socket(self):
        self.clnt.loop_read()
        self.clnt_want_write()

    def clnt_write_socket(self):
        self.clnt.loop_write()
        self.clnt_want_write()

    def clnt_on_subscribe(self, client, userdata, mid, granted_qos):
        self.clnt_subscribed = True

    def clnt_on_message(self, client, userdata, msg):
        llevel = logging.TRACE if hasattr(self.logr, "TRACE") else \
                    logging.DEBUG
        self.logr.log(llevel, f"{self.nm}.clnt_on_message: message=("
            f"{msg.topic}, {msg.payload})")

        # Topic is 'fma/<device>/status'.
        fma_, devnm, status_ = msg.topic.split('/')
        st = { devnm: json.loads(msg.payload) }
        self.device_signals[devnm].status_updated.emit(st)
        self.status_updated.emit(st)

    def clnt_on_disconnect(self, client, userdata, rc):
        self.logr.warning(f"{self.nm} mqtt client disconnected")

    def closeEvent(self, event):
        self.clnt_cleanup()

    def resizeEvent(self, event):
        event.ignore()

    def timerEvent(self, event):
        self.clnt.loop_misc()       # keep mqtt connection alive.
        self.clnt_want_write()

    def post(self, postername, data):
        self.logr.info(f"{self.nm}.post: {postername} posted {data}")

        gmqtt = self.progg['mqtt']
        mqttport = gmqtt['port']
        if gmqtt['secure']:
            tls = { 'ca_certs': gmqtt['cafile'] }
            auth = { 'username': gmqtt['username'],
                        'password': gmqtt['userpasswd'] }
            if mqttport == 0:
                mqttport = 8883    
        else:
            tls, auth = None, None
            if mqttport == 0:
                mqttport = 1883

        if postername == "shutdown":
            payload = json.dumps({'cmd': "shutdown"})
            topic, qos = "fma/controlstatus", 1
        else:
            return

        mqttpublish.single(topic, payload=payload, qos=qos,
            hostname=gmqtt['broker'], port=mqttport, auth=auth, tls=tls)


    def __init__(self, progg):
        # A widget's parent must be a widget. Since the main window is the
        # topmost window, its parent is None.
        super().__init__(parent=None)
        self.progg = progg
        self.logr = logging.getLogger(defaults.LOGRNAME)
        self.nm = "testviewer"

        self.setObjectName(self.nm)
        self.setWindowTitle("Test Viewer")
        self.resize(640, 360)

        # Create the mqtt subscriber and attach the socket notifiers to its
        # socket.
        self.clnt_subscribed = False
        self.clnt = mqtt.Client(client_id=self.nm, clean_session=True)
        self.clnt.on_subscribe = self.clnt_on_subscribe
        self.clnt.on_message = self.clnt_on_message
        self.clnt.on_disconnect = self.clnt_on_disconnect

        gmqtt = self.progg['mqtt']
        broker_port = 1883
        if gmqtt['secure']:
            broker_port = 8883
            self.clnt.tls_set(ca_certs=gmqtt['cafile'])
            self.clnt.username_pw_set(gmqtt['username'],
                gmqtt['userpasswd'])
        self.clnt.connect(gmqtt['broker'], broker_port)
        self.crntf = QSocketNotifier(self.clnt.socket().fileno(),
                        QSocketNotifier.Read, parent=self)
        self.crntf.activated.connect(self.clnt_read_socket)
        self.cwntf = QSocketNotifier(self.clnt.socket().fileno(),
                        QSocketNotifier.Write, parent=self)
        self.cwntf.setEnabled(False)    # See fmqtt.py for why.
        self.cwntf.activated.connect(self.clnt_write_socket)

        # Start a timer.  The timer event is used to call the mqtt client's
        # loop_misc() which keeps the mqtt connection alive.  Its period
        # should be set less than the mqtt keepalive interval (default
        # 60s).
        self.loopmisc_timer = self.startTimer(40000)    # 40s
        if self.loopmisc_timer == 0:
            raise RuntimeError("failed to start timer")

        # Subscribe to topics.
        devices = ['q1', 'q2', 'ed1', 'md', 'ed2', 'q3', 'q4']
        self.topics = [f'fma/{device}/status' for device in devices]
        rc, mid = self.clnt.subscribe([(t, 0) for t in self.topics])
        if rc != mqtt.MQTT_ERR_SUCCESS:
            self.clnt_cleanup()
            raise RuntimeError("failed to subscribe")

        # Create signals for each device in progg['devices']
        self.device_signals = { device: fidgets.DeviceSignal(device, self)
                                    for device in devices }


        # Widgets for the left panel.
        tabpanels = QWidget(parent=self)
        tabpanels.setObjectName("tabpanels")
        tabpanels_layout = QHBoxLayout(tabpanels)
        for _ in range(2):
            tabs = QTabWidget()
            tabs.setDocumentMode(False)
            tabs.setMovable(False)
            tabs.setTabsClosable(False)
            tabs.setUsesScrollButtons(True)
            if 'q1' in devices:
                q1panel = fidgets.MagnetPanel("q1", "Q1", qpanel_setup,
                            self.device_signals["q1"].status_updated,
                            parent=tabs)
                tabs.addTab(q1panel, self.tr("Q1"))
            if 'q2' in devices:
                q2panel = fidgets.MagnetPanel("q2", "Q2", qpanel_setup,
                            self.device_signals["q2"].status_updated,
                            parent=tabs)
                tabs.addTab(q2panel, self.tr("Q2"))
            if 'ed1' in devices:
                ed1ppanel = fidgets.ElectrodePanel("ed1.pos",
                                "ED1+ Electrode", edpanel_setup,
                                self.device_signals['ed1'].status_updated,
                                parent=tabs)
                tabs.addTab(ed1ppanel, self.tr("ED1+"))
                ed1npanel = fidgets.ElectrodePanel("ed1.neg",
                                "ED1- Electrode", edpanel_setup,
                                self.device_signals['ed1'].status_updated,
                                parent=tabs)
                tabs.addTab(ed1npanel, self.tr("ED1-"))
            if 'md' in devices:
                mdpanel = fidgets.MagnetPanel("md", "MD", qpanel_setup,
                            self.device_signals["md"].status_updated,
                            parent=tabs)
                tabs.addTab(mdpanel, self.tr("MD"))
            if 'ed2' in devices:
                ed2ppanel = fidgets.ElectrodePanel("ed2.pos",
                                "ED2+ Electrode", edpanel_setup,
                                self.device_signals['ed2'].status_updated,
                                parent=tabs)
                tabs.addTab(ed2ppanel, self.tr("ED2+"))
                ed2npanel = fidgets.ElectrodePanel("ed2.neg",
                                "ED2- Electrode", edpanel_setup,
                                self.device_signals['ed2'].status_updated,
                                parent=tabs)
                tabs.addTab(ed2npanel, self.tr("ED2-"))
            if 'q3' in devices:
                q3panel = fidgets.MagnetPanel("q3", "Q3", qpanel_setup,
                            self.device_signals["q3"].status_updated,
                            parent=tabs)
                tabs.addTab(q3panel, self.tr("Q3"))
            if 'q4' in devices:
                q4panel = fidgets.MagnetPanel("q4", "Q4", qpanel_setup,
                            self.device_signals["q4"].status_updated,
                            parent=tabs)
                tabs.addTab(q4panel, self.tr("Q4"))
            tabpanels_layout.addWidget(tabs)

        emergency_buttons = fidgets.EmergencyButtons("shutdown", self)

        leftpanel = QWidget(self)
        leftpanel.setObjectName("leftpanel")
        leftpanel_layout = QVBoxLayout(leftpanel)
        leftpanel_layout.addWidget(tabpanels)
        leftpanel_layout.addWidget(emergency_buttons)

        # Widgets for the right panel
        meas_fields_tbl = fidgets.FieldsTbl("meas_fields",
                            "Measured Fields", tbl_setup('meas'),
                            self.status_updated)
        calc_fields_tbl = fidgets.FieldsTbl("calc_fields",
                            "Calculated Fields", tbl_setup('calc'),
                            self.status_updated)
        tables = QWidget(self)
        tables.setObjectName("tables")
        tables_layout = QHBoxLayout(tables)
        tables_layout.addWidget(calc_fields_tbl)
        tables_layout.addWidget(meas_fields_tbl)

        ciform = fidgets.CIForm(self, self.status_updated)

        rightpanel = QWidget(self)
        rightpanel.setObjectName("rightpanel")
        rightpanel_layout = QVBoxLayout(rightpanel)
        rightpanel_layout.addWidget(ciform)
        rightpanel_layout.addWidget(tables)

        # Put left and right panels together
        panels = QWidget(self)
        panels.setObjectName("panels")
        panels_layout = QHBoxLayout(panels)
        panels_layout.addWidget(leftpanel)
        panels_layout.addWidget(rightpanel)

        self.setCentralWidget(panels)


def main():
    app = QApplication([])
    mw = MainWindow(progg)
    mw.show()
    app.exec_()

    
if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-f', '--foreground', action='store_true',
        default=None, help="run in foreground mode")
    parser.add_argument('-v', '--verbose', nargs=0, default=logging.INFO,
        action=utils.VerboseAction, help='verbose mode')
    parser.add_argument('-L', '--logfile', default=None, help="logfilename")
    parser.add_argument('-c', '--conffile', default=None,
        help='toml config file')
    parser.add_argument('-b', '--broker', default=None,
        help='mqtt broker hostname')
    parser.add_argument('-p', '--port', default=0, type=int,
        help="mqtt broker port")
    parser.add_argument('-s', '--secure', action='store_true',
        default=None, help="connect to mqtt broker via ssl")
    parser.add_argument("--cafile", default=None,
        help="CA certificate filename")
    args, unknown = parser.parse_known_args()

    if bool(args.conffile):
        with open(args.conffile, "rb") as finp:
            fprogg = tomllib.load(finp)
        utils.merge_config(fprogg, progg)
        progg['conffile'] = args.conffile
    utils.merge_config(args, progg)     # Merge args into progg.

    # Must have a logfile. 
    progg['logfile'] = args.logfile if bool(args.logfile) else \
                        progg0['logfile']
    logr = logging.getLogger(defaults.LOGRNAME)
    ffmtter = logging.Formatter(datefmt="%y%m%d.%H%M%S",
                fmt="[%(asctime)s.%(msecs)03d <%(levelname)1.1s>] "
                "%(message)s")
    logfile = logging.FileHandler(progg['logfile'], "w")
    logfile.setFormatter(ffmtter)
    logr.addHandler(logfile)
    if progg['foreground']:
        logcons = logging.StreamHandler()
        cfmtter = logging.Formatter(datefmt="%M%S",
                    fmt="[%(asctime)s.%(msecs)03d <%(levelname)1.1s>] "
                    "%(message)s")
        logcons.setFormatter(cfmtter)
        logr.addHandler(logcons)
    logr.setLevel(progg['verbose'])
    main()
