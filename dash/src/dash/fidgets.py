#from PySide2.QtWidgets import QWidget, QFrame, QVBoxLayout, QHBoxLayout,\
#    QLabel, QGroupBox, QGridLayout, QPushButton, QLineEdit, QFormLayout,\
#    QDialog, QDialogButtonBox, QComboBox
#from PySide2.QtGui import QFontMetrics, QPainter, QPen, QBrush, QColor,\
#    QDoubleValidator, QIntValidator
#from PySide2.QtCore import Qt, QSize, QRectF, QPointF, QLineF, Slot,\
#    Signal, QDateTime
#from PySide2.QtCharts import QtCharts

from PySide2.QtCharts import QtCharts
from PySide2.QtCore import Qt, QDateTime, QObject, Signal, Slot
from PySide2.QtGui import QPainter, QPen, QBrush, QDoubleValidator,\
    QFontMetrics, QIntValidator
from PySide2.QtWidgets import QDialog, QFormLayout, QComboBox, QLineEdit,\
    QDialogButtonBox, QFrame, QLabel, QVBoxLayout, QHBoxLayout,\
    QPushButton, QGroupBox, QGridLayout
import defaults
import logging
import time
import utils


# A "named" signal. Since a signal must be defined as a static member of a
# QObject, we can "name" a signal using the QObject's instance name.
# Naming a signal lets us signal slots in device widgets with the same
# name.
#
# Without named signals, the mqtt message handler which converts the status
# message would emit a single generic signal to which all connected slots
# would be run with each slot deciding if the signal data is meant for it.
# Named signals lets us specify which slots should be called.
#
class DeviceSignal(QObject):
    status_updated = Signal(dict)

    def __init__(self, name, parent=None):
        super().__init__(parent=parent)
        self.setObjectName(f"{name}_status_updated")

# Helper function for panel classes.
def label_var_unit(vnm, vlabel, vfmt, vunit):
    label = QLabel()
    label.setObjectName(f"{vnm}_label")
    label.setText(label.tr(vlabel))

    m = utils.printf_re.match(vfmt)
    if m is None:
        raise ValueError(f"{vfmt} - bad format string")
    if m['type_'] == 's' or m['type_'] is None:
        v = ' '
    else:
        v = 0
    var = QLabel()
    var.setObjectName(vnm)
    var.setText(var.tr(vfmt.format(v)))

    unit = QLabel()
    unit.setObjectName(f"{vnm}_unit")
    unit.setText(unit.tr(vunit))

    return (label, var, unit)


class ChartAxesDialog(QDialog):
    def __init__(self, title, parent):
        super().__init__()
        self.setWindowTitle(self.tr(title))
        self.chart = parent

        layout = QFormLayout(self)
        self.time_range_selector = QComboBox()
        time_ranges = []
        for time_range in self.chart.time_ranges:
            minutes = int(time_range/60)
            if minutes == 0: continue
            if minutes <= 60:
                unit = "minute" if minutes == 1 else "minutes"
                time_ranges.append(f"{minutes} {unit}")
            else:
                hours = int(minutes/60)
                time_ranges.append(f"{hours} hours")
        self.time_range_selector.addItems(time_ranges)
        self.time_range_selector.setFrame(True)
        self.time_range_selector.setEditable(False)
        self.time_range_selector.setCurrentIndex(0)
        self.time_range_selector.currentIndexChanged.connect(
            self.time_range_selector.setCurrentIndex)
        layout.addRow(self.tr("Time Axis:"), self.time_range_selector)

        yinput_validator = QDoubleValidator(*self.chart.max_vrange, 2)
        self.ymin_input = QLineEdit()
        self.ymin_input.setText(self.tr("{:6.2f}"
            .format(self.chart.yaxis.min()).strip()))
        self.ymin_input.setValidator(yinput_validator)
        layout.addRow("Ymin", self.ymin_input)

        self.ymax_input = QLineEdit()
        self.ymax_input.setText(self.tr("{:6.2f}"
            .format(self.chart.yaxis.max()).strip()))
        self.ymax_input.setValidator(yinput_validator)
        layout.addRow("Ymax", self.ymax_input)

            
        self.buttons = QDialogButtonBox(
            QDialogButtonBox.Ok|QDialogButtonBox.Cancel, Qt.Horizontal)
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)
        layout.addRow(self.buttons)

    def accept(self):
        tdx = self.time_range_selector.currentIndex()
        ymin = float(self.ymin_input.text())
        ymax = float(self.ymax_input.text())
        if (ymin < ymax and 
                (self.chart.max_vrange[0] <= ymin < 
                self.chart.max_vrange[1]) and (self.chart.max_vrange[0]
                < ymax <= self.chart.max_vrange[1])):
            self.chart.set_time_axis(tdx)
            self.chart.set_y_axis((ymin, ymax))
        super().accept()


# A stripchart to plot measured quantities. Suggested by Darek 20210812 as
# a useful way to monitor possible sparks(voltage drops) in the electric
# dipoles.

class TimeSeriesChart(QtCharts.QChartView):
    # Time ranges in seconds.
    time_ranges = (300, 600, 1800, 3600, 8*3600)
    maxpoints = 30000

    def __init__(self, vlabel, vrange, pencolor=Qt.black, penwidth=1,
            markersize=3.0, framed=False, parent=None):
        super().__init__(parent=parent)
        self.vlabel = vlabel
        self.max_vrange = vrange

        self.chart = QtCharts.QChart()
        self.chart.legend().hide()
        self.chart.setAnimationOptions(QtCharts.QChart.NoAnimation)

        # The time axis. Default range is time_ranges[0].
        self.taxis = None
        self.set_time_axis(0)
        self.chart.addAxis(self.taxis, Qt.AlignBottom)

        self.yaxis = None
        self.set_y_axis(vrange)
        self.chart.addAxis(self.yaxis, Qt.AlignLeft)

        self.series = QtCharts.QScatterSeries(self)
        self.series.setBrush(QBrush(pencolor))
        self.series.setPen(QPen(QBrush(pencolor), penwidth))
        self.series.setMarkerSize(markersize)
        self.chart.addSeries(self.series)
        self.series.attachAxis(self.taxis)
        self.series.attachAxis(self.yaxis)
        self.setChart(self.chart)

        # Chartview settings
        if framed:
            self.setFrameShape(QFrame.Box)
            self.setFrameShadow(QFrame.Sunken)
            self.setLineWidth(1)
        self.setRenderHint(QPainter.Antialiasing)
        self.setMinimumSize(450, 200)


    def set_time_axis(self, trange_idx):
        if self.taxis is None:
            self.taxis = QtCharts.QDateTimeAxis()
            xfont = self.taxis.labelsFont()
            xfont.setPointSize(10)
            self.taxis.setLabelsFont(xfont)
            self.taxis.setTitleText(self.tr("Time"))

        now = QDateTime.currentDateTime()
        nowsecs = now.toSecsSinceEpoch()
        time_range, numdiv = self.time_ranges[trange_idx], 6
        timediv = int(time_range/numdiv)
        nowtick = int(nowsecs - (nowsecs%timediv))
        now.setSecsSinceEpoch(nowtick)
        self.taxis.setRange(now.addSecs(-timediv), now.addSecs(5*timediv))
        self.taxis.setTickCount(numdiv+1)
        self.taxis.setFormat("mm:ss" if time_range <= 3600 else "hh:mm:ss")


    def set_y_axis(self, vrange):
        if self.yaxis is None:
            self.yaxis = QtCharts.QValueAxis()
            xfont = self.yaxis.labelsFont()
            xfont.setPointSize(10)
            self.yaxis.setLabelsFont(xfont)
            self.yaxis.setTitleText(self.tr(self.vlabel))
        self.yaxis.setRange(*vrange)
        self.yaxis.setTickCount(5)


    def append(self, msecs, v):
        tmax = self.taxis.max().toMSecsSinceEpoch()
        if msecs >= tmax:
            dx = self.chart.plotArea().width()/self.taxis.tickCount()
            self.chart.scroll(dx, 0)
        self.series.append(msecs, v)
        if len(self.series.points()) > self.maxpoints:
            self.series.removePoints(0, 600)

    def contextMenuEvent(self, event):
        dialog = ChartAxesDialog("Plot Axes", self)
        dialog.exec_()


class FieldsTbl(QGroupBox):
    def __init__(self, tbl_name, tbl_title, tbl_setup_data,
            data_updated_signal=None, parent=None):
        super().__init__(parent=parent)
        self.logr = logging.getLogger(defaults.LOGRNAME)
        self.tbl_nm = tbl_name
        self.setObjectName(f"{tbl_name}_table")
        self.setTitle(self.tr(tbl_title))
        self.tbl_setup_data = tbl_setup_data
        if isinstance(data_updated_signal, Signal):
            self.data_updated_signal = data_updated_signal
            self.data_updated_signal.connect(self.update_data)

        layout = QGridLayout(self)
        self.vars = {}

        columns = ('label', 'equals', 'varnm', 'unit')
        fmtwidths = { cnm: 0 for cnm in columns }
        rowcnt = 0
        qfm = QFontMetrics(self.font())
        for row, devnm in enumerate(self.tbl_setup_data.keys()):
            dsetup = self.tbl_setup_data[devnm]
            label, var, unit = label_var_unit(dsetup['varnm'],
                                dsetup['label'], dsetup['fmt'],
                                dsetup['unit'])

            label.setAlignment(Qt.AlignLeft)
            var.setAlignment(Qt.AlignRight)
            unit.setAlignment(Qt.AlignLeft)

            equals = QLabel()
            equals.setObjectName(f"{dsetup['varnm']}_equals")
            equals.setText(self.tr('='))
            equals.setAlignment(Qt.AlignCenter)
            
            for cnm in columns:
                m = utils.printf_re.match(dsetup['fmt'])
                fmtwidths[cnm] = max((fmtwidths[cnm], int(m['width'])))

            layout.addWidget(label, row, 0)
            layout.addWidget(equals, row, 1)
            layout.addWidget(var, row, 2)
            layout.addWidget(unit, row, 3)

            self.vars[devnm] = (dsetup['varnm'], var)
            rowcnt += 1

        varnm_width = qfm.boundingRect('4'*fmtwidths['varnm']).width()
        layout.setColumnMinimumWidth(2, varnm_width)

    @Slot(dict)
    def update_data(self, data):
        self.logr.debug(f"{self.tbl_nm}.update_data(): data:\n{data}")


class EmergencyButtons(QFrame):
    def __init__(self, name, viewer):
        super().__init__(parent=viewer)
        self.setObjectName("emergency_buttons")
        self.nm = name
        self.viewer = viewer

        self.setFrameShape(QFrame.Box)
        self.setFrameShadow(QFrame.Sunken)
        self.setLineWidth(1)

        self.setLayout(QHBoxLayout())
        self.vbtn = QPushButton(self.tr("Shutdown Voltages"))
        self.vbtn.clicked.connect(self.vbtn_clicked)
        self.layout().addWidget(self.vbtn)
        self.mbtn = QPushButton(self.tr("Shutdown Magnets"))
        self.mbtn.clicked.connect(self.mbtn_clicked)
        self.layout().addWidget(self.mbtn)

    def vbtn_clicked(self, checked):
        data = { "type": "voltages", "msecs": int(1000.*time.time()) }
        self.viewer.post(self.nm, data)

    def mbtn_clicked(self, checked):
        data = { "type": "magnets", "msecs": int(1000.*time.time()) }
        self.viewer.post(self.nm,  data)


class MagnetPanel(QFrame):
    def __init__(self, device_name, panel_title, panel_setup_data,
            data_updated_signal=None, parent=None):
        super().__init__(parent=parent)
        self.logr = logging.getLogger(defaults.LOGRNAME)
        self.setObjectName(f"{device_name}_panel")
        self.device_name = device_name
        self.panel_setup_data = panel_setup_data
        if isinstance(data_updated_signal, Signal):
            self.data_updated_signal = data_updated_signal
            self.data_updated_signal.connect(self.update_data)
        self.msecs = 0      # data_updated_signal timestamp

        self.setFrameShape(QFrame.Box)
        self.setFrameShadow(QFrame.Sunken)
        self.setLineWidth(1)

        layout = QVBoxLayout(self)
        title = QLabel()
        title.setText(title.tr(panel_title))
        title.setAlignment(Qt.AlignCenter)
        layout.addWidget(title)
        layout.addSpacing(10)

        self.vars = {}
        self.charts = {}
        b_layout = QHBoxLayout()
        for vnm in ('bgoal', 'bmeas'):
            vnm_setup = self.panel_setup_data[vnm]
            label, var, unit = label_var_unit(vnm, vnm_setup['label'],
                vnm_setup['fmt'], vnm_setup['unit'])
            b_layout.addWidget(label)
            b_layout.addStretch()
            b_layout.addWidget(var)
            self.vars[vnm] = var
            b_layout.addWidget(unit)
            if vnm != 'bmeas':
                b_layout.addSpacing(15)
        layout.addLayout(b_layout)

        bmeas_setup = self.panel_setup_data['bmeas']
        self.bchart = TimeSeriesChart('Field (T)', bmeas_setup['meter'],
                        parent=self)
        self.charts['bmeas'] = self.bchart
        layout.addWidget(self.bchart)

        i_layout = QHBoxLayout()
        for vnm in ('iprog', 'imeas'):
            vnm_setup = self.panel_setup_data[vnm]
            label, var, unit = label_var_unit(vnm, vnm_setup['label'],
                vnm_setup['fmt'], vnm_setup['unit'])
            i_layout.addWidget(label)
            i_layout.addStretch()
            i_layout.addWidget(var)
            self.vars[vnm] = var
            i_layout.addWidget(unit)
            if vnm != 'imeas':
                i_layout.addSpacing(15)

        layout.addLayout(i_layout)

        i_layout = QHBoxLayout()
        for vnm in ('vmeas', 'cmeas'):
            vnm_setup = self.panel_setup_data[vnm]
            label, var, unit = label_var_unit(vnm, vnm_setup['label'],
                vnm_setup['fmt'], vnm_setup['unit'])
            i_layout.addWidget(label)
            i_layout.addStretch()
            i_layout.addWidget(var)
            self.vars[vnm] = var
            i_layout.addWidget(unit)
            if vnm != 'cmeas':
                i_layout.addSpacing(15)

        layout.addLayout(i_layout)

    @Slot(dict)
    def update_data(self, data):
        #self.logr.debug(f"{self.device_name}: data={data}")
        if self.device_name not in data:
            return
        #self.logr.info(f"{self.device_name}: data={data}")
        msecs = data[self.device_name]['msecs']
        ddata = data[self.device_name]
        for vnm in self.vars.keys():
            vdata = ddata[vnm][0] if isinstance(ddata[vnm], list) \
                        else ddata[vnm] # ddata[vnm] is a 2-element list
                                        # for a MockCamacController.
            vnm_setup = self.panel_setup_data[vnm]
            self.vars[vnm].setText(self.tr(
                vnm_setup['fmt'].format(vdata)))
            if vnm not in self.charts: continue
            self.charts[vnm].append(msecs, vdata)


class ElectrodePanel(QFrame):
    def __init__(self, device_name, panel_title, panel_setup_data,
            data_updated_signal=None, plotcurrent=False, parent=None):
        '''The device_name convention is ed1.pos, ed1.neg, ed2.pos,
        ed2.neg.'''
        super().__init__(parent=parent)
        self.logr = logging.getLogger(defaults.LOGRNAME)
        self.setObjectName(f"{device_name}_panel")
        self.device_name, self.device_polarity = device_name.split('.')
        self.panel_setup_data = panel_setup_data
        if isinstance(data_updated_signal, Signal):
            self.data_updated_signal = data_updated_signal
            self.data_updated_signal.connect(self.update_data)
        self.msecs = 0      # data_updated_signal timestamp.

        self.setFrameShape(QFrame.Box)
        self.setFrameShadow(QFrame.Sunken)
        self.setLineWidth(1)

        layout = QVBoxLayout(self)
        title = QLabel()
        title.setText(title.tr(panel_title))
        title.setAlignment(Qt.AlignCenter)
        layout.addWidget(title)
        layout.addSpacing(10)

        self.vars = {}
        self.charts = {}


        v0_layout = QHBoxLayout()
        # vprog is in Cary's program but it's not clear to me what the
        # difference is between vprog and vgoal.
        #for vnm in ('vgoal', 'vprog'):
        for vnm in ('vgoal',):
            vnm_setup = self.panel_setup_data[vnm]
            label, var, unit = label_var_unit(vnm, vnm_setup['label'],
                vnm_setup['fmt'], vnm_setup['unit'])
            v0_layout.addWidget(label)
            v0_layout.addStretch()
            v0_layout.addWidget(var)
            self.vars[vnm] = var
            v0_layout.addWidget(unit)
            if vnm == 'vgoal':
                v0_layout.addSpacing(15)
        layout.addLayout(v0_layout)

        vnm = 'vmeas'
        vnm_setup = self.panel_setup_data[vnm]
        label, var, unit = label_var_unit(vnm, vnm_setup['label'],
            vnm_setup['fmt'], vnm_setup['unit'])
        v1_layout = QHBoxLayout()
        v1_layout.addWidget(label)
        v1_layout.addStretch()
        v1_layout.addWidget(var)
        self.vars[vnm] = var
        v1_layout.addWidget(unit)
        layout.addLayout(v1_layout)

        vmeas_setup = self.panel_setup_data['vmeas']
        self.vchart = TimeSeriesChart('Voltage (kV)', vmeas_setup['meter'],
                        parent=self)
        layout.addWidget(self.vchart)
        self.charts['vmeas'] = self.vchart

        i_layout = QHBoxLayout()
        for vnm in ('imeas', 'iprog'):
            vnm_setup = self.panel_setup_data[vnm]
            label, var, unit = label_var_unit(vnm, vnm_setup['label'],
                vnm_setup['fmt'], vnm_setup['unit'])
            i_layout.addWidget(label)
            i_layout.addStretch()
            i_layout.addWidget(var)
            self.vars[vnm] = var
            i_layout.addWidget(unit)
            if vnm == 'imeas':
                i_layout.addSpacing(15)
        layout.addLayout(i_layout)

        if plotcurrent:
            imeas_setup = self.panel_setup_data['imeas']
            self.ichart = TimeSeriesChart('Current (μA)',
                            imeas_setup['meter'], parent=self)
            layout.addWidget(self.ichart)
            self.charts['imeas'] = self.ichart

    @Slot(dict)
    def update_data(self, data):
        #self.logr.debug(f"{self.device_name}.{self.device_polarity}: "
        #    f"data={data}")
        if self.device_name not in data:
            return
        #self.logr.info(f"{self.device_name}.{self.device_polarity}: "
        #    f"data={data}")
        msecs = data[self.device_name]['msecs']
        ddata = data[self.device_name][self.device_polarity]
        for vnm in self.vars.keys():
            vdata = ddata[vnm][0] if isinstance(ddata[vnm], list) \
                        else ddata[vnm] # ddata[vnm] is a list for a
                                        # MockCamacController
            #self.logr.info(f"{self.device_name}.{self.device_polarity}: "
            #    f"vnm={vnm} vdata={vdata}")
            vnm_setup = self.panel_setup_data[vnm]
            self.vars[vnm].setText(self.tr(
                vnm_setup['fmt'].format(vdata)))
            if vnm not in self.charts: continue
            self.charts[vnm].append(msecs, vdata)


class CIForm(QFrame):
    def __init__(self, viewer, data_updated_signal=None):
        super().__init__()
        self.logr = logging.getLogger(defaults.LOGRNAME)
        self.viewer = viewer
        if isinstance(data_updated_signal, Signal):
            self.data_updated_signal = data_updated_signal
            self.data_updated_signal.connect(self.update_data)

        self.setObjectName("central_ion_form")
        self.setFrameShape(QFrame.Box)
        self.setFrameShadow(QFrame.Sunken)
        self.setLineWidth(1)

        self.parameters = {
            'energy': {
                'label': self.tr("Energy (MeV)"),
                'input': None,
                'validator': QDoubleValidator(),
                'fmt': '{:<6.2f}',
                'value': 0.0
            },
            'charge': {
                'label': self.tr("Charge"),
                'input': None,
                'validator': QIntValidator(),
                'fmt': '{:d}',
                'value': 0
            },
            'mass': {
                'label': self.tr('Mass (amu)'),
                'input': None,
                'validator': QIntValidator(),
                'fmt': '{:d}',
                'value': 0
            }
        }
        self.parameters['energy']['validator'].setBottom(0.0)
        self.parameters['charge']['validator'].setBottom(1)
        self.parameters['mass']['validator'].setBottom(1)
        form_layout = QFormLayout()
        form_layout.setObjectName("ciform_form_layout")
        for pnm, p in self.parameters.items():
            p['input'] = QLineEdit(self.tr(
                            p['fmt'].format(p['value']).strip()))
            p['input'].setReadOnly(True)
            form_layout.addRow(self.tr(p['label']), p['input'])

        self.newbtn = QPushButton(self.tr("New"))
        self.newbtn.clicked.connect(self.newbtn_clicked)
        self.okbtn = QPushButton(self.tr("Ok"))
        self.okbtn.clicked.connect(self.okbtn_clicked)
        self.okbtn.setEnabled(False)
        self.cancelbtn = QPushButton(self.tr("Cancel"))
        self.cancelbtn.clicked.connect(self.cancelbtn_clicked)
        self.cancelbtn.setEnabled(False)

        buttons_layout = QHBoxLayout()
        buttons_layout.setObjectName("ciform_buttons_layout")
        buttons_layout.addWidget(self.newbtn)
        buttons_layout.addWidget(self.cancelbtn)
        buttons_layout.addWidget(self.okbtn)

        layout = QVBoxLayout()
        layout.setObjectName("ciform_layout")
        title = QLabel()
        title.setText(title.tr("Central Particle Parameters"))
        title.setAlignment(Qt.AlignCenter)
        layout.addWidget(title)
        layout.addSpacing(10)
        layout.addLayout(form_layout)
        layout.addLayout(buttons_layout)
        self.setLayout(layout)

    def newbtn_clicked(self, checked):
        self.newbtn.setEnabled(False)
        for pnm, p in self.parameters.items():
            p['input'].setReadOnly(False)
            if pnm == 'energy':
                p['input'].selectAll()
                p['input'].setFocus()

        self.okbtn.setEnabled(True)
        self.cancelbtn.setEnabled(True)

    def cancelbtn_clicked(self, checked):
        self.okbtn.setEnabled(False)
        self.cancelbtn.setEnabled(False)
        self.newbtn.setEnabled(True)
        for pnm, p in self.parameters.items():
            p['input'].deselect()
            p['input'].setText(self.tr(p['fmt'].format(p['value']).strip()))
            p['input'].setReadOnly(True)

    def okbtn_clicked(self, checked):
        self.okbtn.setEnabled(False)
        self.cancelbtn.setEnabled(False)
        self.newbtn.setEnabled(True)
        ci = {}
        for pnm, p in self.parameters.items():
            p['input'].deselect()
            p['value'] = float(p['input'].text()) if pnm == 'energy' \
                            else int(p['input'].text())
            p['input'].setReadOnly(True)
            ci[pnm] = p['value']
        self.viewer.post("ci", ci)
        
    @Slot(dict)
    def update_data(self, data):
        if self.okbtn.isEnabled() or 'ci' not in data:
            return
        self.logr.info(f"{self.objectName()} data={data}")
