'''defaults.py

Collection of constants for the fmacp viewer program.  Recommend importing
this module with 'import defaults', not 'from defaults import x', so it is
clear what the variable means.
'''

__author__ = "Ken Teh"
__all__ = [ 'LOGRNAME' ]


LOGRNAME = "logr"
LOGFILE = "fmaviewer.log"
