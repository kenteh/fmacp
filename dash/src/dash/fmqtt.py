'''How to do mqtt with the FMA Qt code.

The FMA Qt code subscribes to mqtt broker that is provided FMA status info
from a C++ executable.  This code contains a test publisher and 2 types of
subscribers: a threaded subscriber which runs the mqtt client and a Qt-ic
subscriber where the Qt event loop handles incoming status messages from
the broker.

The recommendation is not to use threads but to make use of Qt's event
loop and its signal/slot mechanism.

Both methods work. It remains to be tested which is more "better". Better
means that the cpu SHOULD NOT be busy looking for incoming messages from
the mqtt broker. This lets the rest of the GUI update effectively.
'''
from PySide2.QtCore import Qt, QSocketNotifier, Slot, Signal, QObject,\
    QThread
from PySide2.QtWidgets import QApplication, QMainWindow, QLabel
from argparse import ArgumentParser
import defaults
import logging
import os
import paho.mqtt.client as mqtt
import selectors
import signal
import sys
import time
import traceback
import utils

if sys.version_info[1] >= 11:
    import tomllib
else:
    import tomli as tomllib


def reconfig():
    '''Merge command line args into cfg.'''
    global cfg
    cfg['mqtt']['broker'] = args.broker
    cfg['mqtt']['broker_port'] = args.broker_port


def printargs():
    print(f"args:{args}\ncfg={cfg}\nreconfig...")
    reconfig()
    print(f"cfg={cfg}")


# The publisher uses a selector to poll console input.  The selector has a
# large select timeout to reduce spinning in the thread's while loop.
# Because of this, a ^c signal takes a long time to actually stop the
# thread. That's because (1) python threads are masked from signals, and
# (2) the stop requested in the signal handler is not seen quickly because
# of the long select timeout.
#
# The solution to this is to use a pipe and to add the read-end of the pipe
# to the selector.  So, when the signal handler closes the write-end, the
# selector will immediately return thereby allowing the stop request to be
# seen.
class Publisher(utils.Kthread):
    def __init__(self, name, progcfg, prfd=None):
        super().__init__(name=name)
        self.logr = logging.getLogger(defaults.LOGRNAME)
        self.nm = name
        self.prfd = prfd        # pipe read-end descriptor.
        self.cfg = progcfg

    def rdwr(self, sk, sev):
        # Callback for the mqtt client socket; calls the mqtt client's
        # loop_read/loop_write methods.
        pre = f"{self.nm}.rdwr: sk.fd={sk.fd}, sev={sev}"
        if bool(sev & selectors.EVENT_WRITE):
            rc = self.clnt.loop_write()
            self.logr.debug(f"{pre}, loop_write() rc={rc}")
        if bool(sev & selectors.EVENT_READ):
            rc = self.clnt.loop_read()
            self.logr.debug(f"{pre}, loop_read() rc={rc}")

    def readcons(self, sk, sev):
        # Callback for the sys.stdin.  Publishes data to the broker.
        line = sys.stdin.readline()
        if line == '': raise EOFError
        line = line.strip()
        if len(line) == 0: return

        try:
            topic, payload = line.split(maxsplit=1)
            if topic in self.cfg['mqtt']['topics']:
                minfo = self.clnt.publish(topic, payload)
                self.logr.info(f"{self.nm}.readcons: published payload="
                    f'"{payload}" to {topic}, mid={minfo.mid}')
        except:
            self.logr.error(f'"{line}" - bad input')

    def _threadfn(self):
        self.logr.info(f"{self.nm} thread started")
        try:
            ds, skeys = None, []
            mqttcfg = self.cfg['mqtt']
            self.clnt = mqtt.Client(client_id=self.nm, clean_session=True)
            if mqttcfg['secure']:
                self.clnt.tls_set(ca_certs=mqttcfg['cacert'])
                self.clnt.username_pw_set(mqttcfg['username'],
                    mqttcfg['userpasswd'])
            self.clnt.connect(mqttcfg['broker'], mqttcfg['broker_port'])
            ds = selectors.DefaultSelector()
            ssk = ds.register(self.clnt.socket().fileno(),
                    selectors.EVENT_READ, self.rdwr)
            skeys.append(ssk)
            _ = ds.register(sys.stdin, selectors.EVENT_READ, self.readcons)
            skeys.append(_)
            if bool(self.prfd):
                # NOTE: The pipe read-end selector does not have a
                # callback.  This is used as sentinel.
                _ = ds.register(self.prfd, selectors.EVENT_READ)
                skeys.append(_)
            tmout = 30.0
            while True:
                if self.stop_requested:
                    self.logr.info(f"{self.nm} stop requested")
                    break
                if self.clnt.want_write():
                    if bool(ssk.events & selectors.EVENT_WRITE) is False:
                        ssk = ds.modify(ssk.fileobj,
                                selectors.EVENT_READ|selectors.EVENT_WRITE,
                                self.rdwr)
                else:
                    if bool(ssk.events & selectors.EVENT_WRITE) is True:
                        ssk = ds.modify(ssk.fileobj, selectors.EVENT_READ,
                                self.rdwr)
                selected = ds.select(tmout)
                for sk, sev in selected:
                    cb = sk.data
                    if cb is None:
                        # No callback means an I/O event has occurred on
                        # the pipe read-end.  Quit!
                        self.logr.info(f"{self.nm} cb=None, sk.fd={sk.fd}")
                        raise EOFError
                    cb(sk, sev)

                self.clnt.loop_misc()
        except EOFError:
            pass
        except:
            e = sys.exc_info()
            self.logr.error("{} {} exception\n{}".format(self.nm, e[0],
                ''.join(traceback.format_exception(*e))))
        finally:
            if bool(ds) and len(skeys) > 0:
                for sk in skeys:
                    ds.unregister(sk.fileobj)
                ds.close()
            if self.clnt.is_connected():
                self.clnt.disconnect()

        self.logr.info(f"{self.nm} thread exited")


# A kleenex class because we need the kleenex signal handler to hold extra
# data, in this case, the pipe write-end descriptor.
class Kleenex:
    def __init__(self, pipe_wfd):
        self.logr = logging.getLogger(defaults.LOGRNAME)
        self.pipe_wfd = pipe_wfd

    def __call__(self, signr, frame):
        # Close the pipe write-end when signaled.
        self.logr.info(f"signaled <{signr}> close {self.pipe_wfd}")
        os.close(self.pipe_wfd)


def pubmain():
    reconfig()
    prfd, pwfd = os.pipe()
    logr.info(f"os.pipe returns (r,w) = ({prfd}, {pwfd})")
    kleenex = Kleenex(pwfd)
    [signal.signal(s, kleenex) for s in (signal.SIGINT, signal.SIGTERM)]

    try:
        publisher = Publisher("pubber", cfg, prfd=prfd)
        publisher.start()
        while True:
            if not publisher.is_alive():
                logr.error("publisher is not alive")
                break

            time.sleep(2.2)
    except:
        e = sys.exc_info()
        logr.error("{} exception\n{}".format(e[0],
            ''.join(traceback.format_exception(*e))))
    finally:
        publisher.stop()
        publisher.join()
        logr.info("publisher joined")


# Mock up of the various FMA display widgets.  Basically defines a slot
# that can be connected to the status_update signal.
class StatusLabel(QLabel):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.update_status("placeholder")
        self.setAlignment(Qt.AlignCenter)

    @Slot(str)
    def update_status(self, s):
        self.setText(self.tr(s))


# A Qt-ic subscriber.  Threads are discouraged in Qt code. Instead, the
# focus is to use the event loop and signals and slots.  The trick in
# integrating an mqtt client is to wrap a QSocketNotifier around the mqtt
# client's socket. This makes the Qt event loop monitor the socket for I/O
# events. Much like a python selector.
class QSubscriber(QMainWindow):
    sstatus = Signal(str)   # The status_updated signal.

    # Mqtt client callbacks.
    def clnt_on_connect(self, client, userdata, flags, rc):
        self.logr.info(f"{self.nm}.clnt_on_connect: flags={flags}, "
            f"rc={rc}")

    def clnt_on_message(self, client, userdata, msg):
        self.logr.info(f"{self.nm}.clnt_on_message: message=("
            f"{msg.topic}, {msg.payload})")
        self.sstatus.emit(msg.payload.decode())

    def clnt_cleanup(self):
        if self.subscribed:
            self.clnt.unsubscribe(self.cfg['mqtt']['topics'])
        if self.clnt.is_connected():
            self.clnt.disconnect()

    def clnt_want_write(self):
        # A QSocketNotifier works like a selector. For write events, the
        # socket notifier needs to be disabled until there is something to
        # write.
        pre = f"{self.nm}.clnt_want_write:"
        if self.clnt.want_write():
            if not self.cwntf.isEnabled():
                self.cwntf.setEnabled(True)
                self.logr.info(f"{pre} cwntf enabled")
        else:
            if self.cwntf.isEnabled():
                self.cwntf.setEnabled(False)
                self.logr.info(f"{pre} cwntf disabled")

    def wrsck(self):
        rc = self.clnt.loop_write()
        self.logr.info(f"{self.nm}.wrsck: loop_write rc={rc}")

    def rdsck(self):
        rc = self.clnt.loop_read()
        self.logr.info(f"{self.nm}.rdsck: loop_read rc={rc}")
        self.clnt_want_write()

    def timerEvent(self, event):
        self.logr.info(f"{self.nm}.timerEvent: event timer_id="
            f"{event.timerId()}, loopmisc_timer={self.loopmisc_timer}")
        self.clnt.loop_misc()
        self.clnt_want_write()

    def closeEvent(self, event):
        self.clnt_cleanup()

    def __init__(self, progcfg):
        super().__init__(parent=None)   # A widget's parent must be a
                                        # widget. Since the main window is
                                        # the topmost widget, it has no
                                        # parent.

        self.logr = logging.getLogger(defaults.LOGRNAME)
        self.nm = "subber"
        self.cfg = progcfg

        self.setObjectName(self.nm)
        self.setWindowTitle("testing")
        self.resize(500, 200)

        self.label = StatusLabel(parent=self)
        self.sstatus.connect(self.label.update_status)
        self.setCentralWidget(self.label)

        # Set up the mqtt subscriber client.
        mqttcfg = self.cfg['mqtt']
        self.clnt = mqtt.Client(client_id=self.nm, clean_session=True)
        self.clnt.on_connect = self.clnt_on_connect
        self.clnt.on_message = self.clnt_on_message

        if mqttcfg['secure']:
            self.clnt.tls_set(ca_certs=mqttcfg['cacert'])
            self.clnt.username_pw_set(mqttcfg['username'],
                    mqttcfg['userpasswd'])

        self.clnt.connect(mqttcfg['broker'], mqttcfg['broker_port'])

        # After connecting, wrap a read and write socket notifier around
        # the socket.  The Qt event loop will then monitor the socket for
        # I/O events. If there are I/O events, it will emit the activated()
        # signal. This is Qt's version of a python selector.
        self.crntf = QSocketNotifier(self.clnt.socket().fileno(),
                        QSocketNotifier.Read, parent=self)
        self.crntf.activated.connect(self.rdsck)

        # NOTE: A socket is always ready to write unless it is writing.
        # This makes it emit activated() all the time. Disable it until
        # there is something to write.
        # NOTE: This subscriber never writes even when it calls loop_misc()
        # to keep the mqtt connection alive.
        self.cwntf = QSocketNotifier(self.clnt.socket().fileno(),
                        QSocketNotifier.Write, parent=self)
        self.cwntf.setEnabled(False)
        self.cwntf.activated.connect(self.wrsck)

        # Finally, the event loop needs to call the mqtt client's loop_misc
        # periodically within the connection's keepalive interval to
        # maintain the connection. We do this via a timer.  The default
        # keepalive interval is 60s.
        self.loopmisc_timer = self.startTimer(30000)    # 30s
        if self.loopmisc_timer == 0:
            raise RuntimeError("failed to start timer")

        # subscribe to topics
        rc, mid = self.clnt.subscribe([(t, 0) for t in mqttcfg['topics']])
        if rc != mqtt.MQTT_ERR_SUCCESS:
            self.clnt_cleanup()
            raise RuntimeError("failed to subscribe")
        self.subscribed = True


# This part of the code is the threaded subscriber where a FMA gui app
# creates a thread to monitor the mqtt connection.  This Client class
# implements the thread function. It is moved into a QThread.
#
# The client's thread function uses a python selector to monitor the mqtt
# client's socket.  Just like the Publisher, the selector's select()
# timeout makes the app unresponsive to being stopped.  We use the same
# trick as in the Publisher, ie, a pipe.
class Client(QObject):
    sstatus = Signal(str)

    def __init__(self, name, progconfigs, prfd):
        super().__init__(parent=None)   # A QObject that is moved into a
                                        # thread cannot have a parent.
        self.nm = name
        self.logr = logging.getLogger(defaults.LOGRNAME)
        self.cfg = progconfigs
        self.prfd = prfd        # the pipe's read-end.

    def clnt_on_message(self, client, userdata, msg):
        self.logr.info(f"{self.nm}.clnt_on_message: msg=({msg.topic},"
            f"{msg.payload})")
        self.sstatus.emit(msg.payload.decode())

    def clnt_rdwr(self, sk, sev):
        pre = f"{self.nm}.clnt_rdwr: sev={sev}"
        if sev & selectors.EVENT_WRITE:
            rc = self.clnt.loop_write()
            self.logr.info(f"{pre}, loop_write rc={rc}")
        if sev & selectors.EVENT_READ:
            rc = self.clnt.loop_read()
            self.logr.info(f"{pre}, loop_read rc={rc}")

    def clnt_cleanup(self):
        if self.clnt is None: return
        if self.subscribed:
            self.clnt.unsubscribe(self.cfg['mqtt']['topics'])
        if self.clnt.is_connected():
            self.clnt.disconnect()

    def runfn(self):
        # The client's thread function. It uses a selector to monitor the
        # mqtt client's socket for incoming messages from the broker.
        self.logr.info(f"{self.nm} thread started")
        self.clnt = None
        try:
            mqttcfg = self.cfg['mqtt']
            ds, skeys = None, {}
            self.subscribed = False

            # Register the pipe's read-end to selecting.  We do not provide
            # a callback which defaults to None. This is used as a
            # sentinel.
            ds = selectors.DefaultSelector()
            skeys['pipe'] = ds.register(self.prfd, selectors.EVENT_READ)
            self.logr.info(f"{self.nm} registered pipe fd={self.prfd}")

            self.clnt = mqtt.Client(client_id=self.nm, clean_session=True)
            self.clnt.on_message = self.clnt_on_message

            if mqttcfg['secure']:
                self.clnt.tls_set(ca_certs=mqttcfg['cacert'])
                self.clnt.username_pw_set(mqttcfg['username'],
                        mqttcfg['userpasswd'])

            # Registering the mqtt client's socket must be done **after**
            # it connects.
            self.clnt.connect(mqttcfg['broker'], mqttcfg['broker_port'])
            skeys['clnt'] = ds.register(self.clnt.socket().fileno(),
                            selectors.EVENT_READ, self.clnt_rdwr)

            # subscribe to topics
            rc, mid = self.clnt.subscribe(
                        [(t, 0) for t in mqttcfg['topics']])
            if rc != mqtt.MQTT_ERR_SUCCESS:
                self.clnt_cleanup()
                raise RuntimeError("failed to subscribe")
            self.subscribed = True

            tmout = 30.0
            while True:
                if self.thread().isInterruptionRequested():
                    self.logr.info(f"{self.nm} interrupt requested")
                    break

                # A write selector must be disabled until there is
                # something to write. The selector's select() returns
                # immediately when the socket is writable which is usually
                # is unless it is writing.
                if self.clnt.want_write():
                    if not bool(skeys['clnt'].events
                            & selectors.EVENT_WRITE):
                        skeys['clnt'] = ds.modify(skeys['clnt'].fileobj,
                                selectors.EVENT_READ|selectors.EVENT_WRITE,
                                self.clnt_rdwr)
                else:
                    if bool(skeys['clnt'].events & selectors.EVENT_WRITE):
                        skeys['clnt'] = ds.modify(skeys['clnt'].fileobj,
                                selectors.EVENT_READ, self.clnt_rdwr)

                selected = ds.select(tmout)
                for sk, sev in selected:
                    cb = sk.data
                    # The none callback sentinel indicates an I/O event on
                    # the pipe read-end. We assume its write-end was closed
                    # which means we also quit.
                    if cb is None: raise EOFError
                    cb(sk, sev)

                self.clnt.loop_misc()
        except EOFError:
            pass
        except:
            e = sys.exc_info()
            self.logr.error("{} {} exception\n{}".format(self.nm, e[0],
                ''.join(traceback.format_exception(*e))))
        finally:
            for nm, sk in skeys.items():
                ds.unregister(sk.fileobj)
            ds.close()
            self.clnt_cleanup()

        self.logr.info(f"{self.nm} thread exited")
        self.thread().quit()


class TSubscriber(QMainWindow):
    def closeEvent(self, event):
        # The close event is posted when the user clicks on the windows [x]
        # button to close it. On close, we stop the client thread. In order
        # to stop it immediately, we also close the pipe's write-end. The
        # client's thread selector monitors both the mqtt client's socket
        # **and** the pipe. Closing the pipe makes the select() call return
        # immediately so the thread is able to detect the requested
        # interruption.
        if bool(self.clntthrd):
            self.logr.info(f"{self.nm}.closeEvent: closing pwfd={self.pwfd}")
            if self.pwfd >= 0:
                # NOTE: Had to add this guard. For some reason,
                # closeEvent() is called twice.
                os.close(self.pwfd)
                self.pwfd = -1
            self.clntthrd.requestInterruption()
            self.clntthrd.wait()
            self.logr.info(f"{self.nm}.closeEvent: clnt thread waited")

    def __init__(self, progcfg):
        super().__init__(parent=None)   # A widget's parent must be a
                                        # widget. Since the main window is
                                        # the topmost widget, it has no
                                        # parent.

        self.logr = logging.getLogger(defaults.LOGRNAME)
        self.nm = "subber"
        self.cfg = progcfg

        self.setObjectName(self.nm)
        self.setWindowTitle("testing")
        self.resize(500, 200)

        self.label = StatusLabel(parent=self)
        self.setCentralWidget(self.label)

        self.prfd, self.pwfd = os.pipe()
        self.logr.info(f"{self.nm} os.pipe() returns (r,w)="
            f"({self.prfd},{self.pwfd})")

        self.clnt, self.clntthrd = None, None
        self.clntthrd = QThread(parent=self)
        self.clnt = Client(self.nm, self.cfg, self.prfd)
        self.clnt.moveToThread(self.clntthrd)
        self.clntthrd.started.connect(self.clnt.runfn)
        self.clntthrd.finished.connect(self.close)  # Quit mainwindow on
                                                    # thread exit.

        # Connect the client's sstatus signal to the label's update_status
        # slot and start the client thread.
        self.clnt.sstatus.connect(self.label.update_status)
        self.clntthrd.start()


def submain():
    mwclasses = {'qsub': QSubscriber, 'tsub': TSubscriber}
    if not args.main in mwclasses:
        logr.error(f"{args.main} - bad option")
        return

    reconfig()
    app = QApplication([])
    mw = mwclasses[args.main](cfg)
    mw.show()
    app.exec_()


if __name__ == "__main__":
    mains = {'args': printargs, 'pub': pubmain, 'qsub': submain,
                'tsub': submain}
    with open("fmacp.toml", "rb") as finp:
        cfg = tomllib.load(finp)
    mqttcfg = cfg['mqtt']

    parser = ArgumentParser()
    parser.add_argument('-v', '--verbose', nargs=0, default=logging.INFO,
        action=utils.VerboseAction, help='verbose mode')
    parser.add_argument('-L', '--logfile', default=None,
        help='log to file')
    parser.add_argument('-b', '--broker', default=mqttcfg['broker'],
        help='mqtt broker address')
    parser.add_argument('-p', '--broker_port', help='mqtt broker port',
        default=mqttcfg['broker_port'] if 'broker_port' in mqttcfg else
        None, type=int)
    parser.add_argument('-t', '--topics', nargs='+',
        default=mqttcfg['topics'], help='subscribe topics')
    parser.add_argument('-s', '--secure', action='store_true',
        default=mqttcfg['secure'], help='ssl connect to broker')
    parser.add_argument('main', default='args', help='main entry function')
    args = parser.parse_args()

    if args.broker_port is None:
        args.broker_port = 8883 if args.secure else 1883

    logr = logging.getLogger(defaults.LOGRNAME)
    fmtter = logging.Formatter(datefmt="%M.%S",
                fmt="[%(asctime)s.%(msecs)03d <%(levelname)1.1s>] "
                        "%(message)s")
    logcons = logging.StreamHandler()
    logcons.setFormatter(fmtter)
    logr.addHandler(logcons)
    if args.logfile:
        logfile = logging.FileHandler(args.logfile, 'w')
        logfile.setFormatter(fmtter)
        logr.addHandler(logfile)
    logr.setLevel(args.verbose)
    mains[args.main]()
