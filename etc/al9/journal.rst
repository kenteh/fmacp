.. 20230314

Was able to build testdev and testcli successfully on almalinux9.  But requires
building the following packages. They are not available from the al repos. ::

  paho.mqtt.c       1.3.9
  paho.mqtt.cpp     1.2.0
  fmt               9.1.0
  spdlog            1.10.0
  nlohmann/json     3.11.2
  toml11            3.7.1
  ktool             2.3
  modbus            3.1.6

Have saved the _build/CMakeCache.txt files using cmakecache.sh in the cmakecache
folder. ::

  [teh@elrond al9]$ tree cmakecache
  cmakecache
  ├── fmt.txt
  ├── json.txt
  ├── ktool.txt
  ├── paho.mqtt.cpp.txt
  ├── paho.mqtt.c.txt
  ├── spdlog.txt
  └── toml11.txt

  0 directories, 7 files
  [teh@elrond al9]$ 

These files contain the build options set with ccmake. **Note** modbus uses
autotools instead of cmake but there were no build features except for
``--prefix``. 
