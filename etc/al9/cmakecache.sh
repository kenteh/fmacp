# Save CMakeCache.txt files
clonedir=/home/teh/hackware/clones
packages="paho.mqtt.c paho.mqtt.cpp fmt spdlog json toml11 ktool"
for p in $packages; do
    cp -p $clonedir/$p/_build/CMakeCache.txt cmakecache/$p.txt
done
