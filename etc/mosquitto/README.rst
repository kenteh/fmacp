Setting up mosquitto for the FMA
++++++++++++++++++++++++++++++++

Links:

- :ref:`Running mosquitto on macOS <#macos>`

- :ref:`Worked example <#example>`

  - :ref: `TLS files <#tls_files>`


.. _#macos:
Running mosquitto on macOS
==========================

Installed mosquitto via homebrew. While it is possible to start and stop the
mosquitto service, this scenario requires the mosquitto config be located in
/usr/local/etc/mosquitto.

Since I want to use my configs in this folder, do the following ::

  % alias mosquitto='/usr/local/Cellar/mosquitto/2.0.15/sbin/mosquitto

so I can run the server executable directly.

.. _#example:
Worked example
==============

The following setup is used during code development.

Files ::

  /home/teh/hackware/fmacp/etc/mosquitto
  ├── listeners
  │   └── localauth.conf
  ├── mosquitto.conf
  ├── passwd
  ├── pki
  │   └── tls
  │       ├── certs
  │       │   ├── ca.crt
  │       │   └── localsrv.crt
  │       └── private
  │           └── localsrv.key
  └── README.rst

  5 directories, 7 files

Manifest:

mosquitto.conf, localauth.conf
  Configuration used for during development.  Loads localauth.conf which
  specifies CA cert, the server cert and key.  See :ref:`<#tls_files>` for how
  to create the certificate files.

  mosquitto.conf tells mosquitto to include all .conf files. The various
  listener conf files can be disabled simply by changing the file suffix.
  
passwd
  Created with ``mosquitto_passwd``.  Contains the fma user, password (not
  including quotes): "FeqM.dv/dt".

Test the above files by first, starting the broker ::

  [teh@trucker mosquitto]$ pwd
  /home/teh/hackware/fmacp/etc/mosquitto
  [teh@trucker mosquitto]$ mosquitto -c ./mosquitto.conf
  1669928217: Loading config file /home/teh/hackware/fmacp/etc/mosquitto/listeners/localauth.conf
  1669928217: mosquitto version 2.0.15 starting
  1669928217: Config loaded from ./mosquitto.conf.
  1669928217: Opening ipv4 listen socket on port 8883.
  1669928217: mosquitto version 2.0.15 running


Then, in another window, start the subscriber ::

  [teh@trucker mosquitto]$ mosquitto_sub -L mqtts://localhost/home/temp \
  > -i subber -u fma -P 'FeqM.dv/dt' --cafile ./pki/tls/certs/ca.crt

The subscriber will hang waiting for the broker to forward messages from the
publisher.  So, in yet another window, run the following command ::

  [teh@trucker mosquitto]$ mosquitto_pub -L mqtts://localhost/home/temp \
  > -i pubber -u fma -P 'FeqM.dv/dt' --cafile ./pki/tls/certs/ca.crt \
  > -m 18

We should see the mosquitto_sub print the message '18'.


2nd worked example
------------------
Added a second localanon.conf to the listeners folder.  This does not require
TLS and a user/password.  Use as follows. Disable other .conf files in
listeners, then start the mosquitto broker ::

  [teh@trucker mosquitto]$ mosquitto -c ./mosquitto.conf
  1676653424: Loading config file /home/teh/hackware/fmacp/etc/mosquitto/listeners/localanon.conf
  1676653424: mosquitto version 2.0.15 starting
  1676653424: Config loaded from ./mosquitto.conf.
  1676653424: Opening ipv4 listen socket on port 1883.
  1676653424: mosquitto version 2.0.15 running
  (...)

Then, in another window, subscribe to topic ::

  [teh@trucker mosquitto]$ mosquitto_sub -L mqtt://localhost/home/temp -i subber

In a third window, publish some data to the topic ::

  [teh@trucker mosquitto]$ mosquitto_pub -L mqtt://localhost/home/temp -i pubber -m 18

We should see mosquitto_sub print the message '18'.


.. _#tls_files:
TLS files
---------
ca.crt
  See ~/hackware/hwt/ca/easy-rsa/journal.rst for how to create a certificate
  authority.  I use this easy-rsa setup to sign certificate requests.

localsrv.crt, localsrv.key
  Same journal.rst documents how a service creates a private key, uses it to
  create a certificate request, and uses the certificate authority to sign the
  request, thereby generating the server certificate--localsrv.crt.

**Note** Prefer using real certs, eg, via InCommon, when deployed in production.
